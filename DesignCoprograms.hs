-- Playing with some examples from Jeremy Gibbons blog-post 
-- How to design co-programs
-- https://patternsinfp.wordpress.com/2018/11/21/how-to-design-co-programs/

import BasePrelude (delete)

unfold :: (b -> Maybe (a, b)) -> b -> [a]
unfold f u = case f u of
               Nothing     -> []               -- output base case
               Just (x, v) -> x : (unfold f v) -- output cons case

-- zip as unfold
zip' :: ([a], [b]) -> [(a, b)]
zip' = unfold go
    where
        go ([]  , _)    = Nothing
        go (_   , [])   = Nothing
        go (a:as, b:bs) = Just((a, b), (as, bs))

-- zip as fold—is there a better way?
zip'' :: [a] -> [b] -> [(a, b)]
zip'' = foldr f z
    where
        f a g []     = []
        f a g (b:bs) = (a,b):(g bs)
        z = \_ -> []

-- selectSort as unfold
-- see also `ssort` from `Origami.sh`
selectSort :: Ord a => [a] -> [a]
selectSort = unfold go
    where
        go [] = Nothing
        go xs = Just(x, delete x xs)
           where x = minimum xs 
