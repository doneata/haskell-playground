-- https://courses.engr.illinois.edu/cs421/fa2016/mps/MP2/mp2.pdf
import Prelude hiding (sum, map)

import Control.Applicative
import Control.Monad

addK :: Int -> Int -> (Int -> a) -> a
addK x y k = k $ x + y

subK :: Int -> Int -> (Int -> a) -> a
subK x y k = k $ x - y

mulK :: Int -> Int -> (Int -> a) -> a
mulK x y k = k $ x * y

-- Ex. 5.8
-- Compute the expression:
-- > diff p = 2 * ((1 − p) * p)
diffK :: Int -> (Int -> a) -> a
diffK p k =
    subK 1 p $ \d ->
    mulK d p $ \m ->
    mulK 2 m $ \r ->
    k r

-- Ex. 5.9
-- Compute the expression:
-- > quad a b c = (2 * a ** 2 + 4 * b) + c
quadK :: Int -> Int -> Int -> (Int -> a) -> a
quadK a b c k =
    mulK a a $ \x ->
    mulK 2 a $ \x ->
    mulK 4 b $ \y ->
    addK x y $ \z ->
    addK z c $ \r ->
    k r

-- https://www.cs.toronto.edu/~lczhang/324/ex/ex10.pdf
insert :: [Int] -> Int -> [Int]
insert []     y = [y]
insert (x:xs) y = if x > y then y:x:xs else x:(insert xs y)

insertK :: [Int] -> Int -> ([Int] -> r) -> r
insertK [] y k     = k [y]
insertK (x:xs) y k = if x > y then k (y:x:xs) else insertK xs y (\ res -> k (x:res))
-- k (x:insert xs y)
-- (λ zs → k (x:zs)) (insert xs y)
-- insertK xs y (λ zs → k (x:zs))

if' :: Bool -> a -> a -> a
if' True  t _ = t
if' False _ f = f

ifK :: Bool -> a -> a -> (a -> r) -> r
ifK b t f k = if b then k t else k f
-- ifK True  t _ k = k t
-- ifK False _ f k = k f

-- f x = 1 + g x
-- gK x k = k (g x)
-- fK x k = k (f x)
--        = k (1 + g x)
--        = (λ r → k (1 + r)) (g x)
--        = gK x (λ r → k (1 + r))

-- f x = h (g x)
-- gK x k = k (g x)
-- hK x k = k (h x)
-- fK x k = gK x  $ λ x' →
--          hK x' $ λ x″ →
--           k x″

sum :: [Int] -> Int
sum []     = 0
sum (x:xs) = x + sum xs

sumK :: [Int] -> (Int -> r) -> r
sumK []     k = k 0
sumK (x:xs) k = sumK xs (\ r -> k (x + r))

fact :: Integer -> Integer
fact 0 = 1
fact n = n * fact (n - 1)

factK :: Integer -> (Integer -> r) -> r
factK 0 k = k 1
factK n k = factK (n - 1) (\ r -> k (n * r))

-- defunctionalize
data Kont n = Done | Next n  -- F - = 1 + -

factK' :: Integer -> Kont Integer -> Integer
factK' 0 k = apply k 1
factK' n k = factK' (n - 1) (Next (apply k n))

apply :: Kont Integer -> Integer -> Integer
apply Done     i = i
apply (Next n) i = n * i

-- data Filter = IsOdd | LessThan Int | And Filter Filter
--
-- filter1 :: Filter -> [Int] -> [Int]
-- filter1 f []     = []
-- filter1 f (x:xs) = let xs' = filter1 f xs in if apply f x then x:xs' else xs'
-
-- apply :: Filter -> Int -> Bool
-- apply IsOdd        a = odd a
-- apply (LessThan n) a = a <= n
-- apply (And p q)    a  = (apply p a) && (apply q a)

map :: (a -> b) -> [a] -> [b]
map f []     = []
map f (x:xs) = (f x):(map f xs)

mapK :: (a -> b) -> [a] -> ([b] -> r) -> r
mapK f []     k = k []
mapK f (x:xs) k = mapK f xs (\ r -> k (f x:r))

filterK :: (a -> Bool) -> [a] -> ([a] -> r) -> r
filterK f []     k = k []
filterK f (x:xs) k = filterK f xs (\ xs' -> if f x then k (x: xs') else k xs')

foldrK :: (a -> b -> b) -> b -> [a] -> (b -> r) -> r
foldrK f b []     k = k b
foldrK f b (x:xs) k = foldrK f b xs (\ b' -> k (f x b'))

reverseK :: [a] -> ([a] -> r) -> r
reverseK []     k = k []
reverseK (x:xs) k = reverseK xs (\ r -> k (r ++ [x]))

-- data Lam a r = Lam ([a] -> r)
reverseK' :: [a] -> ([a] -> r) -> r
reverseK' []     k = k []
reverseK' (x:xs) k = reverseK' xs (\ r -> k (r ++ [x]))

-- https://www.seas.upenn.edu/~cis552/13fa/hw/hw07/index.html
data Cont r a = Cont { runCont :: (a -> r) -> r }

instance Monad (Cont r) where
   return x = Cont (\k -> k x)
   m >>= f  = Cont (\k -> runCont m (\a -> runCont (f a) k))

instance Applicative (Cont r) where
  pure = return
  (<*>) = ap

instance Functor (Cont r) where
  fmap = (<*>) . pure

map' :: (a -> b) -> [a] -> Cont r [b]
map' f []     = return []
map' f (x:xs) = map' f xs >>= (\ r -> return (f x:r))

filter' :: (a -> Bool) -> [a] -> Cont r [a]
filter' f []     = return []
filter' f (x:xs) = do
    xs' <- filter' f xs
    if f x then return (x:xs') else return xs'
