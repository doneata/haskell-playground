-- http://www.haskellforall.com/2013/02/you-could-have-invented-comonads.html

-- §1 The builder pattern

type Option = String
data Config = MkConfig [Option] deriving Show

-- Builders
configBuilder :: [Option] -> Config
configBuilder = MkConfig

defaultConfig :: [Option] -> Config
defaultConfig opts = configBuilder $ ["-Wall"] ++ opts

someBuilder :: [Option] -> Config
someBuilder = undefined

-- Setters
profile :: ([Option] -> Config) -> Config
profile builder = builder ["-prof", "-auto-all"]

goFaster :: ([Option] -> Config) -> Config
goFaster builder = builder ["-O2"]

-- Appenders
profile'  :: ([Option] -> Config) -> ([Option] -> Config)
profile' builder = \ opts -> builder $ ["-prof", "-auto-all"] ++ opts

goFaster' :: ([Option] -> Config) -> ([Option] -> Config)
goFaster' builder = \opts -> builder $ ["-O2"] ++ opts

-- Utility functions
extract :: ([Option] -> Config) -> Config
extract builder = builder []

extend :: (([Option] -> Config) -> Config) -> ([Option] -> Config) -> ([Option] -> Config)
extend setter builder = \ opts2 -> setter (\ opts1 -> builder $ opts1 ++ opts2)

(#) :: a -> (a -> b) -> b
(#) = flip ($)

-- Examples:
-- > defaultConfig # profile' # goFaster' # extract
-- > defaultConfig # extend profile # extend goFaster # extract

-- Property 1:
-- > extract (extend setter builder) = setter builder
--
-- Proof:
-- extract (extend setter builder)
--  = { definition of extend }
-- extract (\ opts2 -> setter (\ opts1 -> builder $ opts1 ++ opts2))
--  = { definition of extract }
-- (\ opts2 -> setter (\ opts1 -> builder $ opts1 ++ opts2)) []
--  = { function application: bind opts2 to [] } 
-- setter (\ opts1 -> builder $ opts1 ++ [])
--  = { list is monoid with [] as identity: xs ++ [] = xs = [] ++ xs }
-- setter (\ opts1 -> builder opts1)
--  = { \ a -> f a = f }
-- setter builder


