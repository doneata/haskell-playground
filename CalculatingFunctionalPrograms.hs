{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE UndecidableInstances #-}
-- J. Gibbons. Calculating functional programs

import Prelude hiding (curry, uncurry, Num)

-- Identity functor
data I a = I { unI :: a }
    deriving (Show, Eq)

instance Functor I where
    fmap f = I . f . unI

-- Constant functor
data K a b = K { unK :: a }
    deriving (Show, Eq)

instance Functor (K a) where
    fmap _ = K . unK

-- Composition functor
data Comp f g a = Comp { unComp :: f (g a) }
    deriving (Show, Eq)

instance (Functor f, Functor g) => Functor (Comp f g) where
    fmap f = Comp . fmap (fmap f) . unComp

class Bifunctor f where
    bimap :: (a -> b) -> (c -> d) -> f a c -> f b d

-- Projection bifunctor
data Proj a b = Proj { unProj :: a }
    deriving (Show, Eq)

instance Bifunctor Proj where
    bimap f _  = Proj . f . unProj

-- Sectioning functor -- I'm unsure about the current implementation
data Sect x a b = Sect { unSect :: x a b }
    deriving (Show, Eq)

instance Bifunctor x => Functor (Sect x a) where
    fmap f = Sect . bimap id f . unSect

-- Lifting functor
data Lift x f g a = Lift { unLift :: x (f a) (g a) }

instance Show (x (f a) (g a)) => Show (Lift x f g a) where
    show (Lift x) = "(Lift " ++ show x ++ ")"

instance (Bifunctor x, Functor f, Functor g) => Functor (Lift x f g) where
    fmap f = Lift . bimap (fmap f) (fmap f) . unLift

-- Product bifunctor
data Prod a b = Prod a b
    deriving (Show, Eq)

-- Fork
(△) :: (a -> b) -> (a -> c) -> a -> Prod b c
(f △ g) a = Prod (f a) (g a)

infixr 6 △
fork = (△)  -- alias

exl :: Prod a b -> a
exl (Prod a _) = a

exr :: Prod a b -> b
exr (Prod _ b) = b

uncurry :: (a -> b -> c) -> Prod a b -> c
uncurry f (Prod a b) = f a b

curry :: (Prod a b -> c) -> a -> b -> c
curry f a b = f (Prod a b)

-- Product map
(×) :: (a -> a') -> (b -> b') -> Prod a b -> Prod a' b'
f × g = (f . exl) △ (g . exr)

infixr 5 ×
cross = (×)  -- alias

instance Bifunctor Prod where
    bimap = (×)

-- Coproduct bifunctor
data Coprod a b = Inl a | Inr b
    deriving (Show, Eq)

-- Join
(▽) :: (a -> c) -> (b -> c) -> Coprod a b -> c
(f ▽ _) (Inl a) = f a
(_ ▽ g) (Inr b) = g b
join = (▽)  -- alias

-- Coproduct map
(†) :: (a -> a') -> (b -> b') -> Coprod a b -> Coprod a' b'
f † g = (Inl . f) ▽ (Inr . g)
dsum = (†)

instance Bifunctor Coprod where
    bimap = (†)

-- Distributivity
distl :: Prod a (Coprod b c) -> Coprod (Prod a b) (Prod a c)
distl (Prod a (Inl b)) = Inl (Prod a b)
distl (Prod a (Inr c)) = Inr (Prod a c)

-- Booleans
type MyBool = Coprod () ()

true' :: MyBool
true' = Inl ()

false' :: MyBool
false' = Inr ()

--
guard :: (a -> MyBool) -> a -> Coprod a a
guard p = (exl † exl) . distl . (id △ p)

--
data Fix f = Fix { unFix :: f (Fix f) }

instance Show (f (Fix f)) => Show (Fix f) where
    show (Fix x) = "(Fix (" ++ show x ++ "))"

-- List
data ListF a b = ListF { unListF :: Lift Coprod (K ()) (Lift Prod (K a) I) b }
    deriving Show

data List a = List { unList :: Fix (ListF a) }
    deriving Show

nil :: List a
nil = (List . Fix . ListF . Lift . Inl . K) ()

cons :: Prod a (List a) -> List a
cons = List . Fix . ListF . Lift . Inr . Lift . (K × I . unList)

cons' :: a -> List a -> List a
cons' = curry cons

wrap :: a -> List a
wrap a = cons' a nil

unCons :: List a -> Prod a (List a)
unCons = _ . unLift . unListF . unFix . unList

-- fold :: (f a b -> b) -> Fix f a

-- §4.1 A simple compiler
data Op = Add
        | Mul
        | Num Int
    deriving (Show, Eq)

type Val = Int

data Expr = Expr { unExpr :: Fix (Lift Prod (K Op) List) }
    deriving Show

val :: Int -> Expr
val = Expr . Fix . Lift . (K . Num △  const nil)

binaryOp :: Op -> Expr -> Expr -> Expr
binaryOp op e1 e2 = Expr . Fix . Lift $ Prod (K op) vals
    where
        into x xs = cons' (unExpr x) xs
        vals = into e1 (into e2 nil)

add = binaryOp Add
mul = binaryOp Mul

apply :: Op -> List Val -> Val
apply Add     xs = undefined
apply Mul     xs = undefined
apply (Num v) _  = v

-- eval :: Expr -> Val
-- eval = foldExpr apply

-- Appendix A. Digraphs
--  ×   /\
--  △   uT
--  ▽   dT
--  †   /-
