-- Utrecht – Advanced Functional Programming course (2021)
-- Assignment 1
-- http://www.cs.uu.nl/docs/vakken/afp/assignment1.html

import Control.Arrow ((***))
import Debug.Trace (trace)

-- In this assignment we want to build a library to generate smooth
-- permutations. Given a list of integers xs and an integer d, a smooth
-- permutation of xs with maximum distance d is a permutation in which the
-- difference of any two consecutive elements is at less than d.

-- A naïve implementation just generates all the permutations of a list,

split :: [a] -> [(a, [a])]
split []     = []
split (x:xs) = (x, xs) : [(y, x:ys) | (y, ys) <- split xs]

perms :: [a] -> [[a]]
perms []     = [[]]
perms xs     = [(v:p) | (v, vs) <- split xs, p <- perms vs]

-- and then filters out those which are smooth,

smooth :: (Ord a, Num a) => a -> [a] -> Bool
smooth n (x:y:ys) = abs (y - x) < n && smooth n (y:ys)
smooth _ _        = True

smoothPerms :: Int -> [Int] -> [[Int]]
smoothPerms n xs = filter (smooth n) (perms xs)

-- A better approach is to build a tree, for which it holds that each path from
-- the root to a leaf corresponds to one of the possible permutations, next
-- prune this tree such that only smooth paths are represented, and finally use
-- this tree to generate all the smooth permutations from.

data PermTree a = Leaf | Forest [(a, PermTree a)]
    deriving (Show, Eq)

unfoldPermTree :: (s -> Either () [(a, s)]) -> s -> PermTree a
unfoldPermTree next x = case next x of
                          Left ()  -> Leaf
                          Right xs -> Forest $ map (id *** unfoldPermTree next) xs

listToPermTree :: [a] -> PermTree a
listToPermTree = unfoldPermTree next
    where
        next [] = Left ()
        next xs = Right $ split xs

smoothPermsTree :: Int -> [Int] -> PermTree Int
smoothPermsTree n xs = unfoldPermTree next (Nothing, xs)
    where
        -- use Maybe Int to retain previous seen value
        next (_, []) = Left ()
        next (Nothing, xs) = Right $ map wrap $ split xs
        next (Just p,  xs) = Right $ map wrap $ filter (isCloseTo p) $ split xs
        wrap (a, as)  = (a, (Just a, as))
        isCloseTo p x = abs (p - fst x) < n

permTreeToPerms :: PermTree a -> [[a]]
permTreeToPerms Leaf        = [[]]
permTreeToPerms (Forest xs) = concat $ map (\ (a, t) -> map (a:) (permTreeToPerms t)) xs

-- data Tree a = Leaf a | Node (Tree a) (Tree a)
--     deriving Show
-- 
-- unfoldTree :: (s -> Either a (s, s)) -> s -> Tree a
-- unfoldTree next x = case next x of
--                       Left  y      -> Leaf y
--                       Right (l, r) -> Node (unfoldTree next l) (unfoldTree next r)
-- 
-- balanced :: Int -> Tree ()
-- balanced = unfoldTree next
--     where
--         next 0 = Left ()
--         next n = Right (n - 1, n - 1)
-- 
-- sized :: Int -> Tree Int
-- sized n = unfoldTree next (0, n)
--     where
--         next (s, 0) = Left s
--         next (s, n) = Right ((s, l), (s + l + 1, r))
--             where
--                 m = n - 1
--                 l = m `div` 2
--                 r = m - l
