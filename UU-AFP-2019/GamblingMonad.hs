-- An exericse from the summer school on Advanced Functional Programming
-- https://uu-afp.github.io/as1.html#monads-for-a-gambling-game
import Control.Monad
import Data.Ratio
import System.Random

data Coin = H | T deriving (Show, Enum)
data Dice = D1 | D2 | D3 | D4 | D5 | D6 deriving (Show, Enum)
data Outcome = Win | Lose deriving (Show, Enum)

class Monad m => MonadGamble m where
    toss :: m Coin
    roll :: m Dice

isHead :: Coin -> Bool
isHead H = True
isHead T = False

isWin :: Outcome -> Bool
isWin Win  = True
isWin Lose = False

diceVal :: Dice -> Int
diceVal d = 1 + fromEnum d

-- Same "game" is used for both approaches!
game :: MonadGamble m => m Outcome
game = do
    toss' <- sequence $ take 6 $ repeat toss
    roll' <- roll
    let coin = length $ filter isHead toss'
    let dice = diceVal roll'
    let outcome = if dice >= coin then Win else Lose
    return outcome

-- Part 1: Simulation
instance Random Coin where
    random g = let (i, g') = randomR (0, 1) g in (toEnum i, g')
    randomR (c, c') g = let (i, g') = randomR (fromEnum c, fromEnum c') g
                        in (toEnum i, g')

instance Random Dice where
    random g = let (i, g') = randomR (0, 5) g in (toEnum i, g')
    randomR (c, c') g = let (i, g') = randomR (fromEnum c, fromEnum c') g
                        in (toEnum i, g')

instance MonadGamble IO where
    toss = do
        g <- getStdGen
        let (v, g') = random g
        setStdGen g'
        return v
    roll = do
        g <- getStdGen
        let (v, g') = random g
        setStdGen g'
        return v

simulate :: IO Outcome -> Int -> IO (Ratio Int)
simulate game n = do
    results <- sequence $ take n $ repeat game
    let numWins = length $ filter isWin results
    return $ numWins % n

-- Part 2: Decision trees
data DecisionTree a = Result a | Decision [DecisionTree a]
    deriving Show

instance Functor DecisionTree where
    fmap f (Result a)    = Result (f a)
    fmap f (Decision as) = Decision (fmap (fmap f) as)

instance Monad DecisionTree where
    -- DecisionTree a → (a → DecisionTree b) → DecisionTree b
    (Result a)    >>= f = f a
    (Decision as) >>= f = Decision $ fmap (>>= f) as

instance Applicative DecisionTree where
    pure a = Result a
    -- (<*>) :: f (a → b) → f a → f b
    (<*>) = ap
    -- TODO
    -- Result f    <*> Result a    = Result (f a)
    -- Result f    <*> Decision as = fmap f (Decision as)
    -- Decision fs <*> Result a    = Decision $ fmap (fmap (flip ($) a)) fs
    -- Decision fs <*> Decision as = Decision $ fmap (fmap _ fs) as

instance MonadGamble DecisionTree where
    toss = Decision $ map (Result . toEnum) [0..1]
    roll = Decision $ map (Result . toEnum) [0..5]

probabilityOfWinning :: DecisionTree Outcome -> Ratio Int
probabilityOfWinning tree = numWins % numPoss
    where
        numWins = length $ filter isWin leaves
        numPoss = length leaves
        leaves  = getLeaves tree

getLeaves :: DecisionTree a -> [a]
getLeaves (Result a)    = [a]
getLeaves (Decision as) = concat $ map getLeaves as
