-- https://uu-afp.github.io/as1.html#instrumented-state-monad

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}

import Control.Applicative (Applicative(..))
import Control.Monad       (liftM, ap)
import Data.Monoid         ((<>))

-- Exercise 1: Give default implementations of get and put in terms of modify,
-- and a default implementation of modify in terms of get and put.
class Monad m => MonadState m s | m -> s where
    get :: m s
    get = modify id

    put :: s -> m ()
    put s = do
        modify (const s)
        return ()

    modify :: (s -> s) -> m s
    modify f = do
        s <- get
        put (f s)
        return s

data Counts = Counts { binds   :: Int
                     , returns :: Int
                     , gets    :: Int
                     , puts    :: Int
                     }
    deriving (Show, Eq)

-- Exercise 2: As a convenience, give a Monoid instance for Count that sums the
-- counts pairwise. 
instance Monoid Counts where
    mempty = Counts 0 0 0 0
    mappend (Counts b r g p) (Counts b' r' g' p') =
        Counts (b + b') (r + r') (g + g') (p + p')

oneBind, oneReturn, oneGet, onePut :: Counts
oneBind   = Counts 1 0 0 0
oneReturn = Counts 0 1 0 0
oneGet    = Counts 0 0 1 0
onePut    = Counts 0 0 0 1

newtype State' s a = State' { runState' :: (s, Counts) -> (a, s, Counts) }

-- Exercise 3: Give Monad and MonadState instances for State' that count the
-- number of (>>=), return, get and put operations.
instance Functor (State' s) where
    fmap = liftM

instance Applicative (State' s) where
    pure = return
    (<*>) = ap

instance Monad (State' s) where
    return a = State' $ \ (s, c) -> (a, s, c <> oneReturn)
    st >>= k = State' $ \ (s, c) ->
        let
            (a, s' , c' ) = runState' st    (s , c )
            (b, s'', c'') = runState' (k a) (s', c')
        in (b, s'', c'' <> oneBind)

instance MonadState (State' s) s where
    get   = State' $ \ (s, c) -> (s , s, c <> oneGet)
    put s = State' $ \ (_, c) -> ((), s, c <> onePut)

data Tree a = Branch (Tree a) a (Tree a) | Leaf
    deriving (Show, Eq)

-- Exercise 4: Write a function that labels a tree with integers increasingly,
-- using a depth-first in-order traversal.
label :: MonadState m Int => Tree a -> m (Tree (Int, a))
label Leaf           = return Leaf
label (Branch l a r) = do
    l' <- label l
    r' <- label r
    v  <- get
    put (v + 1)
    return $ Branch l' (v, a) r'

-- Exercise 5: Write a function that runs a state monadic computation in the
-- instrumented state monad, given some initial state of type s, and returns
-- the computed value and the number of operations counted.
run :: State' s a -> s -> (a, Counts)
run state s = let (a, _, c) = runState' state (s, mempty) in (a, c)

-- > run (label tree1) 42
tree1 = Branch (Branch Leaf "B" Leaf) "A" Leaf
tree2 = Branch (Branch Leaf "B" (Branch Leaf "C" Leaf)) "A" Leaf
