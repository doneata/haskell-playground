-- https://en.wikibooks.org/wiki/Haskell/Zippers

data Node a = DeadEnd a
            | Passage a (Node a)
            | Fork    a (Node a) (Node a)
    deriving (Show, Eq)

data Branch a = KeepStraightOn a
              | TurnLeft a (Node a)
              | TurnRight a (Node a)
    deriving (Show, Eq)

type Thread a = [Branch a]

type Zipper a = (Thread a, Node a)

maze :: Node (Int, Int)
maze =
    Fork (0, 2)
        (Fork (-2, 0)
            (DeadEnd (-1,  0))
            (DeadEnd ( 0, -2)))
        (Passage (2, 0)
            (Fork (1, 0)
                (Passage (0,  1) (DeadEnd (0, 0)))
                (DeadEnd (0, -1))))

z = ([] , maze)
        
get :: Zipper a -> a
get (_, DeadEnd a)   = a
get (_, Passage a _) = a
get (_, Fork a _ _)  = a

put :: a -> Zipper a -> Zipper a
put a (as, DeadEnd _)   = (as, DeadEnd a)
put a (as, Passage _ n) = (as, Passage a n)
put a (as, Fork _ m n)  = (as, Fork a m n)

update :: (a -> a) -> Zipper a -> Zipper a
update f n = put (f $ get n) n

keepStraightOn :: Zipper a -> Maybe (Zipper a)
keepStraightOn (as, Passage a n) = Just ((KeepStraightOn a):as, n)
keepStraightOn _                 = Nothing

turnRight :: Zipper a -> Maybe (Zipper a)
turnRight (as, Fork a l r) = Just ((TurnRight a l):as, r)
turnRight _                = Nothing

turnLeft :: Zipper a -> Maybe (Zipper a)
turnLeft (as, Fork a l r) = Just ((TurnLeft a r):as, l)
turnLeft _                = Nothing

back :: Zipper a -> Maybe (Zipper a)
back ([]                    , _)  = Nothing
back ((KeepStraightOn a):as , n) = Just (as, Passage a n)
back ((TurnRight a r):as    , n) = Just (as, Fork a n r)
back ((TurnLeft a l):as     , n) = Just (as, Fork a l n)

-- Example:
-- > turnRight z >>= keepStraightOn
