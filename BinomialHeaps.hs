-- Based on Hinze's paper, "Explaining binomial heaps"
import Data.List (delete)

data MinView q a = Min a (q a)
                 | Infty
    deriving (Eq, Show)

data BinaryTree a = Empty
                  | Bin a (BinaryTree a) (BinaryTree a)
    deriving (Eq, Show)

data ToppedTree a = ToppedTree { getToppedTree :: MinView BinaryTree a }
    deriving (Eq, Show)

class PriorityQueue q where
    emptyQ   :: Ord a => q a
    toQ      :: Ord a => a -> q a
    insertQ  :: Ord a => a -> q a -> q a
    (|+|)    :: Ord a => q a -> q a -> q a
    splitMin :: Ord a => q a -> MinView q a
    insertQ a q = toQ a |+| q

instance PriorityQueue [] where
    emptyQ = []
    toQ a = [a]
    (|+|) = (++)
    splitMin [] = Infty
    splitMin xs = Min m (delete m xs)
        where m = foldr1 min xs

instance PriorityQueue ToppedTree where
    emptyQ = ToppedTree Infty
    toQ a  = ToppedTree $ Min a Empty
    (ToppedTree Infty)     |+| q = q
    p                      |+| (ToppedTree Infty) = p
    (ToppedTree (Min a s)) |+| (ToppedTree (Min b t)) | a <= b    = ToppedTree (Min a (Bin b t s))
                                                      | otherwise = ToppedTree (Min b (Bin a s t))
    splitMin (ToppedTree Infty) = Infty
    splitMin (ToppedTree (Min a q)) = Min a (secondBest q)
        where
            secondBest Empty       = ToppedTree Infty
            secondBest (Bin v l r) = ToppedTree (Min v l) |+| secondBest r

q :: [Int]
q = foldr insertQ emptyQ [10,9..1]

q' :: ToppedTree Int
q' = foldr insertQ emptyQ [10,9..1]
