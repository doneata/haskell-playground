-- Exercise 10.6 from "Functional Programming in Scala"
-- Writing foldr and foldl in terms of hom (also known as foldMap)

import Data.Monoid

hom :: Monoid m => (a -> m) -> ([a] -> m)
hom f = mconcat . map f

foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' f b xs = appEndo (hom (Endo . f) xs) b

foldl' :: (b -> a -> b) -> b -> [a] -> b
foldl' f b xs = appEndo (hom (Endo . flip f) xs) b
