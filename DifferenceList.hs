-- See also the paper by Rivas and Jaskelioff, Notions of Computation as Monoids
-- https://arxiv.org/abs/1406.4823
newtype DList a = DList ([a] -> [a])

toDList :: [a] -> DList a
toDList xs = DList (xs ++)

fromDList :: DList a -> [a]
fromDList (DList leftAction) = leftAction []

instance Monoid (DList a) where
    mappend (DList f) (DList g) = DList $ \ xs -> f xs ++ g xs
    mempty = DList id

-- fromList (d1 <> d2)
-- = { <> }
-- fromList (DList $ \ xs -> unDlist d1 xs ++ unDlist d2 xs)
-- = { fromList }
-- (\ xs -> unDlist d1 xs ++ unDlist d2 xs) []
-- = { function application }
-- unDlist d1 [] ++ unDlist d2 []
-- = { fromDList }
-- fromDList d1 ++ fromDList d2
