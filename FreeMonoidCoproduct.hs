-- See Awodey, Category Theory, example 3.5
{-# LANGUAGE TypeOperators #-}

-- Showing that the free monoid, M(-), preserves coproducts, that is,
-- M(A + B) ≅ M(A) + M(B)

import Data.Monoid

-- The coproduct of free monoids
data a :+: b = C { unC :: [Either a b] }
    deriving (Show, Eq)

injL :: [a] -> a :+: b
injL = C . hom (eta . Left)

injR :: [b] -> a :+: b
injR = C . hom (eta . Right)

coprod :: Monoid n => ([a] -> n) -> ([b] -> n) -> a :+: b -> n
coprod f g = hom (coprodE (f . eta) (g . eta)) . unC

-- Utility functions
eta :: a -> [a]
eta a = [a]

hom :: Monoid m => (a -> m) -> [a] -> m
hom f = foldMap f

coprodE :: (a -> c) -> (b -> c) -> Either a b -> c
coprodE f _ (Left  a) = f a
coprodE _ g (Right b) = g b

-- Example:
-- coporod (has 0) (has 'a') . injL = has 0
-- coporod (has 0) (has 'a') . injR = has 'a'
has :: Eq a => a -> [a] -> Any
has a = hom $ Any . (== a)
