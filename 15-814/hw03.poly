% task 3
type nat = !a. (a -> a) -> a -> a

decl zero : nat
defn zero = /\a. \s. \z. z

decl succ : nat -> nat
defn succ = \n. /\a. \s. \z. s (n [a] s z)

decl plus : nat -> nat -> nat
defn plus = \n. \k. /\a. \s. \z. n [a] s (k [a] s z)

decl times : nat -> nat -> nat
defn times = \n. \k. /\a. \s. \z. n [a] (k [a] s) z

norm _0 = zero
norm _1 = succ _0
norm _2 = succ _1
norm _3 = succ _2

% test cases for plus
norm _4  = plus _3 _1
norm _5  = plus _2 _3
norm _10 = plus _5 _5

% test cases for times
norm __6  = times _2 _3
norm __0  = times _0 _5
norm __10 = times _5 _2

% task 4
type bool = !a. a -> a -> a

decl true : bool
defn true = /\a. \t. \f. t

decl false : bool
defn false = /\a. \t. \f. f

decl omega1 : bool -> bool -> bool
defn omega1 = \x. x [bool] x

norm _tt = omega1 true  true  % → true
norm _tf = omega1 true  false % → true
norm _ft = omega1 false true  % → true
norm _ff = omega1 false false % → false

% omega1 corresonds to logical or

type nat1 = !a. a -> (a -> a) -> a

decl omega2 : nat1 -> (nat1 -> nat1) -> nat1
defn omega2 = \x. x [nat1] x

decl zero1 : nat1
defn zero1 = /\a. \z. \s. z

decl succ1 : nat1 -> nat1
defn succ1 = \n. /\a. \z. \s. s (n [a] z s)

norm _0_1 = zero1
norm _1_1 = succ1 _0_1
norm _2_1 = succ1 _1_1
norm _3_1 = succ1 _2_1

decl id : !a. a -> a
defn id = /\a. \x. x

norm a = omega2 _0_1 (id [nat1])
norm b = omega2 _1_1 (id [nat1])
norm c = omega2 _2_1 (id [nat1])
norm d = omega2 _0_1 (succ1)
norm e = omega2 _1_1 (succ1)
norm f = omega2 _2_1 (succ1)

% task 2

type b = _
type c = _
decl fun_ii : (!a. (c -> !x. x)) -> c -> !a. b
defn fun_ii = \x. \y. /\a. x [a -> a] y [b]
