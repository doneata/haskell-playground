decl id : !a. a -> a
defn id = /\a. \x. x

type nat = !a. (a -> a) -> a -> a

decl zero : nat
defn zero = /\a. \s. \z. z

decl succ : nat -> nat
defn succ = \n. /\a. \s. \z. s (n [a] s z)

decl plus : nat -> nat -> nat
defn plus = \n. \k. n [nat] succ k

norm _0 = zero
norm _1 = succ _0
norm _2 = succ _1
norm _3 = succ _2
norm _4 = succ _3
norm _5 = succ _4
norm _6 = succ _5

% task 2

type shrub = !a. (a -> a -> a) -> (nat -> a) -> a

decl node : shrub -> shrub -> shrub
defn node = \t1. \t2. /\a. \n. \l. n (t1 [a] n l) (t2 [a] n l)

decl leaf : nat -> shrub
defn leaf = \x. /\a. \n. \l. l x

decl s1 : shrub
defn s1 = node (leaf _1) (node (leaf _2) (leaf _3))

decl s2 : shrub
defn s2 = node (node (leaf _3) (leaf _2)) (leaf _1) 

decl sumup : shrub -> nat
defn sumup = \t. t [nat] plus (id [nat])

decl mirror : shrub -> shrub
defn mirror = \t. t [shrub] (\t1. \t2. node t2 t1) leaf

conv _6 = sumup s1
conv _6 = sumup s2

conv s1 = mirror s2
conv s2 = mirror s1

% task 3

type tan = !a. a -> (a -> a) -> a

decl forth : nat -> tan
defn forth = \n. /\a. \z. \s. n [a] s z

decl back : tan -> nat
defn back = \t. /\a. \s. \z. t [a] z s

decl bf : nat -> nat
norm bf = \x. back (forth x)
% defn bf = \x. /\a. \s. \z. x [a] s z

decl fb : tan -> tan
norm fb = \y. forth (back y)
% defn fb = \y. /\a. \z. \s. y [a] z s

% we need η-conversion to have equality to the identity function
%   bf = id [nat]
%   fb = id [tan]
