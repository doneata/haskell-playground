% booleans
defn true  = \x. \y. x
defn false = \x. \y. y

% natural numbers
defn zero = \s. \z. z
defn succ = \n. \s. \z. s (n s z)

norm one   = succ zero
norm two   = succ one
norm three = succ two

defn iter  = \f. \z. \n. n f z
defn plus  = iter succ
defn times = \n. iter (plus n) zero

norm four   = plus two two
norm five   = succ four
norm six    = succ five
norm seven  = succ six
norm nine   = plus six three
norm ten    = succ nine
norm eleven = succ ten
norm twelve = times three four

% pairs
defn pair = \x. \y. \k. k x y
defn fst = \p. p true
defn snd = \p. p false

% primitive recursion
% primrec : (Nat → a → a) → a → Nat → a
defn primrec = \f. \z. \n. snd (iter (\p. pair (succ (fst p)) (f (fst p) (snd p))) (pair zero z) n)
defn pred = \n. primrec (\x. \y. x) zero n

defn I = \x. x
defn K = \x. \y. x

defn Y = \h. (\x. h (x x)) (\x. h (x x))

(*
if0 0     c d = c
if0 (n+1) c d = d
*)

defn if0 = \n. \c. \d. n (K d) c

(*
lucas 0       = 2
lucas 1       = 1
lucas (n + 2) = lucas (n + 1) + lucas n
*)

defn lucashelper = \f. \n. if0 n two (if0 (pred n) one (plus (f (pred n)) (f (pred (pred n)))))
defn lucas = Y lucashelper

conv lucas zero   = two
conv lucas one    = one
conv lucas nine   = plus six (times seven ten)
conv lucas eleven = pred (times two (times ten ten))

norm _ = lucas zero
norm _ = lucas one
norm _ = lucas nine
norm _ = lucas eleven
