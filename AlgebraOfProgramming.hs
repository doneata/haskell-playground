import Prelude hiding (nil, succ, product)

-- TODO Polynomial functors
data Product   f g a = Pair (f a) (g a)      deriving (Show, Eq)
data Coproduct f g a = InL (f a) | InR (g a) deriving (Show, Eq)

outl :: (a, b) -> a
outl = fst

outr :: (a, b) -> b
outr = snd

fork :: ((c -> a), (c -> b)) -> c -> (a, b)
fork (f, g) c = (f c, g c)

inl :: a -> Either a b
inl = Left

inr :: b -> Either a b
inr = Right

cofork :: ((a -> c), (b -> c)) -> Either a b -> c
cofork (f, g) (Left  a) = f a
cofork (f, g) (Right b) = g b

-- Natural numbers
data Nat    = Zero  | Succ  Nat deriving (Show, Eq) -- inital type
type NatF a = Either () a                           -- and its functor
-- data NatF a = I () :+: I a

zero :: () -> Nat
zero _ = Zero

one :: () -> Nat
one _ = Succ Zero

succ :: Nat -> Nat
succ n = Succ n

alphaNat :: NatF Nat -> Nat
alphaNat (Left ()) = Zero
alphaNat (Right n) = Succ n

sumNat :: (Nat, Nat) -> Nat
sumNat (m, n) = cataNat (cofork (\ () -> m, succ)) n

prodNat :: (Nat, Nat) -> Nat
prodNat (m, n) = cataNat (cofork (zero, \ n -> sumNat (m, n))) n

-- instance Functor NatF where
--     fmap f ZeroF     = ZeroF
--     fmap f (SuccF a) = SuccF (f a)
--
cataNat :: (NatF a -> a) -> Nat -> a
cataNat f = f . fmap (cataNat f) . inv
    where
        inv Zero     = Left ()
        inv (Succ n) = Right n

-- Lists
data List  a   = Nil  | Cons  a (List a) deriving (Show, Eq) -- inital type
data ListF a b = NilF | ConsF (a, b)     deriving (Show, Eq) -- and its functor

instance Functor (ListF a) where
    fmap f NilF           = NilF
    fmap f (ConsF (a, b)) = ConsF (a, f b)

nil :: () -> List a
nil _ = Nil

cons :: a -> List a -> List a
cons = Cons

cataList :: (ListF b a -> a) -> List b -> a
cataList f = f . fmap (cataList f) . inv
    where
        inv Nil         = NilF
        inv (Cons x xs) = ConsF (x, xs)

coforkList :: ((() -> c), ((d, e) -> c)) -> ListF d e -> c
coforkList (f, g) NilF      = f ()
coforkList (f, g) (ConsF p) = g p

-- Exercise 3.6
preds :: Nat -> List Nat
preds = outl . cataNat (cofork (fork (nil, zero), k))
    where
        k (ns, n) = (cons (succ n) ns, succ n)

-- Exercise 3.7
product :: List Nat -> Nat
product = cataList (coforkList (one, prodNat))
