{-# LANGUAGE InstanceSigs #-}
-- Code based on Chris Taylor's implementation, which can be found here:
-- https://gist.github.com/chris-taylor/4745921

-- import Control.Monad.State.Lazy
import Control.Monad.Trans.RWS.Lazy

data IOAction a = Return a
                | Put String (IOAction a)
                | Get (String -> IOAction a)

instance Functor IOAction where
    fmap :: (a -> b) -> IOAction a -> IOAction b
    fmap f (Return a) = Return (f a)
    fmap f (Put s io) = Put s (fmap f io)
    fmap f (Get g)    = Get (\s -> fmap f (g s))

instance Applicative IOAction where
    pure = Return
    -- TODO
    (<*>) = undefined

instance Monad IOAction where
    (>>=) :: IOAction a -> (a -> IOAction b) -> IOAction b
    (Return a) >>= f = f a
    (Put s io) >>= f = Put s $ io >>= f
    (Get g)    >>= f = Get $ \s -> (g s) >>= f

instance Show a => Show (IOAction a) where
  show io = go 0 0 io
    where
      go m n (Return a) = ind m "Return " ++ show a
      go m n (Put s io) = ind m "Put " ++ show s ++ " (\n" ++ go (m+2) n io ++ "\n" ++ ind m ")"
      go m n (Get g)    = let i = "$" ++ show n
                          in ind m "Get (" ++ i ++ " -> \n" ++ go (m+2) (n+1) (g i) ++ "\n" ++ ind m ")"
      ind m s = replicate m ' ' ++ s

-- Utils
get' :: IOAction String
get' = Get Return

put' :: String -> IOAction ()
put' s = Put s (Return ())

-- Sample programs
echo :: IOAction ()
echo = get' >>= put'

hello :: IOAction ()
hello = put' "What is your name?"      >>= \_    ->
        get'                           >>= \name ->
        put' "What is your age?"       >>= \_    ->
        get'                           >>= \age  ->
        put' ("Hello " ++ name ++ "!") >>= \_    -> 
        put' ("You are " ++ age ++ " years old")

hello2 :: IOAction ()
hello2 = do put' "What is your name?"
            name <- get'
            put' "What is your age?"
            age <- get'
            put' ("Hello, " ++ name ++ "!")
            put' ("You are " ++ age ++ " years old!")

-- Run the programs
run :: IOAction a -> IO a
run (Return a) = return a
run (Put s io) = putStrLn s >> run io
run (Get f)    = getLine >>= run . f

-- toState :: IOAction a -> State String a
-- toState (Return a) = state $ \s -> (a ,s)
-- toState (Put s io) = state (\s' -> ((), s' ++ " " ++ s)) >> toState io
-- toState (Get f)    = toState (f "...")

type IOActionRWS = RWS [String] () [String]

runRWS :: IOAction a -> IOActionRWS a
runRWS (Return a) = rws $ \ r s -> (a, s, ())
runRWS (Put s io) = undefined
runRWS (Get f)    = undefined
