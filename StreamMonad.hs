data Stream a = Cons a (Stream a)

takeStream :: Int -> Stream a -> [a]
takeStream 0 _           = []
takeStream n (Cons a as) = a : takeStream (n - 1) as

instance Show a => Show (Stream a) where
    show = unwords . fmap show . takeStream 10

instance Functor Stream where
    fmap f (Cons x xs) = Cons (f x) (fmap f xs)

instance Applicative Stream where
    pure a                      = Cons a (pure a)
    (Cons f fs) <*> (Cons a as) = Cons (f a) (fs <*> as)

instance Monad Stream where
    xs >>= f = diag (fmap f xs)

diag :: Stream (Stream a) -> Stream a
diag (Cons xs xss) = Cons (hd xs) (diag (fmap tl xss))
    where
        hd (Cons a _ ) = a
        tl (Cons _ as) = as

inc :: Int -> Stream Int
inc n = Cons n $ inc (n + 1)

zeros :: Stream Int
zeros = pure 0

ex :: Stream Int
ex = do
    x <- inc 0 -- 0 1 2 3 4  ...
    y <- inc x -- 0 2 4 6 8  ...
    z <- inc y -- 0 3 6 9 12 ...
    return z
