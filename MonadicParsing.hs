-- Graham Hutton. "Programming in Haskell."
-- §13. Monadic parsing

import Control.Applicative
import Data.Char

newtype Parser a = P (String -> [(a, String)]) -- use Maybe?

parse :: Parser a -> String -> [(a, String)]
parse (P p) inp = p inp

item :: Parser Char
item = P $ \ s -> case s of
    ""   -> []
    x:xs -> [(x, xs)]

fork :: (c -> a) -> (c -> b) -> (c -> (a, b))
fork f g c = (f c, g c)

instance Functor Parser where
    fmap f p = P $ fmap (fork (f . fst) (id . snd)) . parse p

instance Applicative Parser where
    pure a    = P $ \ s -> [(a, s)]
    pf <*> pa = P $ \ s -> case parse pf s of
        []        -> []
        [(f, s')] -> parse (fmap f pa) s'

three :: Parser (Char, Char)
three = pure (\ a b c -> (a, c)) <*> item <*> item <*> item

instance Monad Parser where
    pa >>= f = P $ \ s -> case parse pa s of
        []        -> []
        [(a, s')] -> parse (f a) s'

instance Alternative Parser where
    empty     = P $ const []
    pa <|> pb = P $ \ s -> case parse pa s of
        []        -> parse pb s
        [(a, s')] -> [(a, s')]

sat :: (Char -> Bool) -> Parser Char
sat f = do
    x <- item
    if f x then return x else empty 

digit :: Parser Char
digit = sat isDigit

lower :: Parser Char
lower = sat isLower

upper :: Parser Char
upper = sat isUpper

letter :: Parser Char
letter = sat isAlpha

alphanum :: Parser Char
alphanum = sat isAlphaNum

char :: Char -> Parser Char
char x = sat (== x)

string :: String -> Parser String
string [] = return []
string (x:xs) = do
    char x
    string xs
    return (x:xs) 
