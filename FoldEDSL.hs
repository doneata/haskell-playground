-- See the paper by Jeremy Gibbons and Nicholas Wu, Folding domain-specific languages
-- https://dl.acm.org/doi/10.1145/2628136.2628138
{-# LANGUAGE DeriveFunctor #-}

cross :: (a -> b, c -> d) -> (a, c) -> (b, d)
cross (f, g) = \ (a, c) -> (f a, g c)

fork :: (c -> a, c -> b) -> c -> (a, b)
fork (f, g) = \ c -> (f c, g c)

type Size  = Int
type Width = Int
type Depth = Int

data CircuitF a = IdentityF Size
                | FanF Size
                | AboveF a a
                | BesideF a a
                | StretchF [Size] a
    deriving (Eq, Functor, Show)

type Circuit = (Width, Depth)

widthAlg :: CircuitF Width -> Width 
widthAlg (IdentityF s) = s
widthAlg (FanF s) = s
widthAlg (AboveF x y) = x
widthAlg (BesideF x y) = x + y
widthAlg (StretchF x y) = x + y

depthAlg :: CircuitF Depth -> Depth
depthAlg (IdentityF s) = s
depthAlg (FanF s) = s
depthAlg (AboveF x y) = x
depthAlg (BesideF x y) = x + y
depthAlg (StretchF x y) = x + y

wdAlg :: CircuitF Circuit -> Circuit
wdAlg = cross (widthAlg, depthAlg) . fork (fmap fst, fmap snd)

identity :: Size -> Circuit
identity s = wdAlg (IdentityF s)

fan :: Size -> Circuit
fan s = wdAlg (FanF s)

circuit1 :: Circuit
circuit1 = undefined

-- TODO
-- [ ] Finish examples above
-- [ ] Define interpretation in terms of monoid operation
-- [ ] Define parameterized interpretations, i.e., Circuit = CircuitAlg a -> a
