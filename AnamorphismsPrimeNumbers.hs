{-# LANGUAGE DeriveFunctor #-}

type Coalgebra f a = a -> f a

data StreamF e a = StreamF e a
    deriving Functor

fstS :: StreamF e a -> e
fstS (StreamF e _) = e

sndS :: StreamF e a -> a
sndS (StreamF _ a) = a

data Fix f = Fix (f (Fix f))

unFix :: Fix f -> f (Fix f)
unFix (Fix f) = f

ana :: Functor f => Coalgebra f a -> a -> Fix f
ana coalg = Fix . fmap (ana coalg) . coalg

-- Get the first `n` elements of a fixed stream.
takeS :: Int -> Fix (StreamF a) -> [a]
takeS n = fmap (fstS . unFix) . take n . iterate (sndS . unFix)

-- Generates multiples of a given number. Example:
-- > takeS 20 $ ana multiples (2, 1)
multiples :: Coalgebra (StreamF Int) (Int, Int)
multiples (p, i) = StreamF (p * i) (p, i + 1)

-- Generates prime numbers using Eratosthenes' sieve. Example:
-- > takeS 20 $ ana eratosthenes [2..]
eratosthenes :: Coalgebra (StreamF Int) [Int]
eratosthenes (p:xs) = StreamF p [x | x <- xs, x `mod` p /= 0]
