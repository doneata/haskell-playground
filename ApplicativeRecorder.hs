-- An exercise in Applicative design by Ionuț G. Stan
-- https://gist.github.com/igstan/d461eceaed2369095dae1243f484db67
{-# LANGUAGE DeriveFunctor #-}

import Control.Applicative
import Control.Monad.Writer
import Control.Monad.Reader
import Control.Monad.Identity

-- The typeclass based on which we write our domain-specific "scripts".
class DSL m where
  upload :: m String
  finish :: m String

-- ----------------------------------------------------------------------------
-- Sample "scripts" using the DSL.
--
-- The Applicative constraint allows us to traverse the whole "script" AST,
-- without actually executing it. Monad would be too much of a constraint.
-- ----------------------------------------------------------------------------
computationA :: (Applicative m, DSL m) => m String
computationA =
  markCompleted "A" <$> upload <*> finish

computationB :: (Applicative m, DSL m) => m String
computationB =
  markCompleted "B"
    <$> (markCompleted "B.1" <$> upload <*> finish)
    <*> (markCompleted "B.2" <$> upload <*> finish)

markCompleted :: String -> String -> String -> String
markCompleted name a b =
  name ++ " computed from: " ++ a ++ "; " ++ b ++ "."

-- We can now use the "scripts" in a recording manner — we first traverse
-- them and log/record all emitted instructions, we obtain that list of
-- instructions from a Writer (which acts as the log implementation), but
-- we also obtain a distributing Reader. This reader expects a result based
-- on the instructions emitted by the writer, which will be available once
-- we optimize and actually execute all logged instructions.
main =
  let
    -- Define a composed applicative computation.
    composed = markCompleted "All" <$> computationA <*> computationB

    -- Time to traverse the applicative computation tree, recording the
    -- instructions present in it and building a big reader that will accept
    -- the result of executing the optimized query, which optimized query
    -- will result from processing the logged instructions.
    --
    -- The final reader is called `distribute` because it distributes results
    -- back to the original computations.
    recorded = runRecorder (composed :: Recorder String)
    (distribute, instructions) = runIdentity . runWriterT $ recorded
  in do
    putStrLn ("All instructions: " ++ show instructions)
    let message = "global instruction count = " ++ show (length instructions)
    putStrLn . runReader distribute . GlobalResult $ message

-- ----------------------------------------------------------------------------
-- The Instruction-Recording Interpreter
-- ----------------------------------------------------------------------------

-- First, we need an ADT representing the operations of the DSL.
data Instr =
  Upload | Finish deriving (Eq, Show)

-- This is the result of the global computation, i.e., the computation of
-- all the script instructions, merged together and, probably, optimized in
-- some way. It's "global" to denote the global optimizations that we can
-- perform on the DSL "scripts" seen as a whole.
newtype GlobalResult = GlobalResult { result :: String }

-- The datatype backing our smart Applicative and DSL instances.
newtype Recorder a =
  Recorder { runRecorder :: Writer [Instr] (Reader GlobalResult a) }
  deriving Functor

-- This is the tricky part. Our Recorder is a Writer which accumulates
-- instructions and *computes a Reader*, which Reader takes a result and
-- gives it back to the original recorded computation, finally producing
-- the result of the computation.
instance Applicative Recorder where
  -- A pure value is one that doesn't use the global result, nor logs anything.
  pure a = Recorder (writer (reader (\_ -> a), []))

  -- Applying a recorded computation to another recorded computation means
  -- obtaining the final reader of the first computation and applying it to
  -- the final reader of the second computation. The writers are sequenced
  -- using monadic bind.
  (Recorder recordedF) <*> (Recorder recordedA) =
    Recorder (recordedF >>= (\f -> fmap (\a -> f <*> a) recordedA))

instance DSL Recorder where
  -- This is a pretty basic implementation which just logs the instruction
  -- associtated with a DSL operation AND forwards the global result back to
  -- the underlying computations. In a real-world scenario, you'll probably
  -- want to send down just parts of the global result, based on arguments of
  -- the `upload` or `finish` methods, which will be closed over.
  --
  -- For exampe, if `upload` takes a `host` parameter, you'd lookup a value
  -- associated with that host in the global result.
  upload = Recorder (writer (reader (\r -> result r), [Upload]))
  finish = Recorder (writer (reader (\r -> result r), [Finish]))
