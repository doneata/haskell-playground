-- | An investigation into the @Traversable@ typeclass, based on the
-- Typeclassopedia
-- https://wiki.haskell.org/Typeclassopedia#Traversable
{-# LANGUAGE DeriveFunctor #-}

import Data.Functor.Constant (Constant(..))
import Data.Functor.Identity (Identity(..))
import Data.Monoid (Sum(..))
import Data.Traversable
import Control.Applicative (ZipList(..))

data Tree a = Leaf | Node a (Tree a) (Tree a)
    deriving (Eq, Show, Functor)

instance Foldable Tree where
    foldMap f Leaf         = mempty
    foldMap f (Node a l r) = f a <> foldMap f l <> foldMap f r

instance Traversable Tree where
    sequenceA Leaf          = pure Leaf
    sequenceA (Node fa l r) = Node <$> fa <*> sequenceA l <*> sequenceA r

-- TODO Check whether this instance is valid.
-- I'm surprised by the claimed found online that this type of binary tree doesn't
-- admit @Monad@ instances. See the following discussions:
-- * https://stackoverflow.com/a/6799885/474311
-- * https://www.reddit.com/r/haskell/comments/cb1j40/is_there_a_valid_monad_instance_for_binary_trees/
instance Applicative Tree where
    pure a = Node a Leaf Leaf
    -- | Substitutes the shape of the second tree into the first one.
    Leaf         <*> _            = Leaf
    _            <*> Leaf         = Leaf
    -- _            <*> Leaf         = Leaf
    -- (Node f l r) <*> (Node a x y) = Node (f a) (l <*> x) (r <*> y)
    (Node f l r) <*> t = subLeaves (fmap f t) (l <*> t) (r <*> t)
        where
            subLeaves (Node a Leaf Leaf) x y = Node a x y
            subLeaves (Node a Leaf r)    x y = Node a x (subLeaves r x y)
            subLeaves (Node a l    Leaf) x y = Node a (subLeaves l x y) y
            subLeaves (Node a l    r)    x y = Node a (subLeaves l x y) (subLeaves r x y)

class Functor f => Monoidal f where
    unit  :: f ()
    (***) :: f a -> f b -> f (a,b)

instance Monoidal Tree where
    unit    = pure ()
    t *** p = pure (,) <*> t <*> p

-- | Tree with a zippy applicative instance.
data ZipTree a = Leaf' | Node' a (ZipTree a) (ZipTree a)
    deriving (Eq, Show, Functor)

instance Applicative ZipTree where -- @pure@ builds an infinte binary tree with the given value.
    pure a                          = t where t = Node' a t t
    Leaf'         <*> _             = Leaf'
    _             <*> Leaf'         = Leaf'
    (Node' f l r) <*> (Node' a x y) = Node' (f a) (l <*> x) (r <*> y)

-- | Two ways to turn a tree of lists into a list of trees—based on how the
-- effects are sequenced by the @Applicative@ instance corresponding to lists:
--   1. all combinations
t1 :: Tree [Int]
t1 = Node [1, 2, 3] (Node [4, 5] (Node [6] Leaf Leaf) (Node [7] Leaf Leaf)) (Node [8] Leaf Leaf)
-- > sequenceA t1

--   2. zipping
t2 :: Tree (ZipList Int)
t2 = Node (ZipList [1, 2, 3]) (Node (ZipList [4, 5]) (Node (ZipList [6]) Leaf Leaf) (Node (ZipList [7]) Leaf Leaf)) (Node (ZipList [8]) Leaf Leaf)
-- > sequenceA t2

-- | Turning list of trees into a tree of list. In this case the tree specifies
-- the effects and the list specifies the structure to traverse. Again we
-- obtain different answer by choosind different ways of sequencing the
-- effects.
l1 :: [Tree Int]
l1 = [Node 1 (Node 2 Leaf Leaf) (Node 3 Leaf Leaf), Node 4 (Node 5 Leaf Leaf) Leaf]
-- > sequenceA l1

l2 :: [ZipTree Int]
l2 = [Node' 1 (Node' 2 Leaf' Leaf') (Node' 3 Leaf' Leaf'), Node' 4 (Node' 5 Leaf' Leaf') Leaf']
-- > sequenceA l2

p1 = Node 1 (Node 2 Leaf Leaf) (Node 3 Leaf Leaf)
p2 = Node 4 (Node 5 Leaf Leaf) Leaf
p3 = Node 6 Leaf (Node 7 Leaf Leaf)
-- Check Applicative laws
-- > unit *** p1    ≅ p1
-- > p1   *** unit  ≅ p1
-- > (p1 *** p2) *** p3 ≅ p1 *** (p2 *** p3)

-- | Composing traverse twice lets us go under two layers of @Traversable@s,
-- similar to @fmap . fmap@ composition. This can be seen from its type:
-- traverse . traverse :: (a -> f b) -> t (t' a) -> f (t (t' b))

-- | Implementing @traverse@ and @sequenceA@ in terms of each other.
traverse' :: (Applicative f, Traversable t) => (a -> f b) -> t a -> f (t b)
traverse' f = sequenceA . fmap f

sequenceA' :: (Applicative f, Traversable t) => t (f a) -> f (t a)
sequenceA' = traverse id

-- | Implementing @fmap@ and @foldMap@ in terms of @Traversable@ methods.
fmap' :: Traversable t => (a -> b) -> t a -> t b
fmap' f = runIdentity . traverse (Identity . f)
-- > fmap' (+1) p1

foldMap' :: (Traversable t, Monoid m) => (a -> m) -> t a -> m
foldMap' f = getConstant . traverse (Constant . f)
-- > foldMap' Sum p1

-- | Why @Set@ is not @Traversable@?
-- Not sure; somewhat handwavy: because @Traversable@ need a fixed shape, while
-- @Set@ cannot specify the shape—it can store the elements in any order.

-- | Extra exercise, from Haskell Wiki.
-- > The @mapAccumL@ function behaves like a combination of @fmap@ and @foldl@;
-- > it applies a function to each element of a structure, passing an
-- > accumulating parameter from left to right, and returning a final value of
-- > this accumulator together with the new structure.
-- The underlying Applicative is the State data type.

-- Defined the output in the reverse order (that is, @(s, a)@ instead of
-- @(a, s)@) in order to better match the type of @mapAccumL@.
data State s a = State { runState :: s -> (s, a) }
    deriving Functor

instance Applicative (State s) where
    pure a = State $ \ s -> (s, a)
    sf <*> sa = State $ \ s -> let
        (s',  f)  = runState sf s
        (s'', a) = runState sa s'
        in (s'', f a)

mapAccumL :: Traversable t => (a -> b -> (a, c)) -> a -> t b -> (a, t c)
mapAccumL f a t = runState (traverse (State . flip f) t) a

-- If using the standard @State@ monad we need to swap back and forth the
-- arguments to match the type @s -> (a, s)@.
-- > import Control.Monad.Trans.State
-- > import Data.Tuple (swap)
-- > mapAccumL step z t = swap (runState (traverse (\ b -> state (\ a -> swap (step a b))) t) z)
