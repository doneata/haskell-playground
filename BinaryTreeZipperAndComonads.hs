-- https://stackoverflow.com/a/25530311/474311

{-# LANGUAGE DeriveFunctor #-}

import Control.Comonad

data LinLZ a = LinLZ
    {  subListCtxt  :: [a]
    ,  subList      :: [a]
    }

-- Plugs a sublist back into its context, reversing back up the path.
plugLinLZ :: LinLZ a -> [a]
plugLinLZ (LinLZ []     ys) = ys
plugLinLZ (LinLZ (x:xs) ys) = plugLinLZ $ LinLZ xs (x:ys)

type DL a =
    (  [a]  -- The sublist context for the node where the element is
    ,  [a]  -- The tail of the node where the element is
    )

data ZL a = ZL
    {  this :: a
    ,  between :: DL a
    }  deriving (Show, Eq, Functor)

-- Plugs an element back into its context.
outZL :: ZL a -> [a]
outZL (ZL y (xs, ys)) = plugLinLZ $ LinLZ xs (y:ys)

-- Given a list, we can pair each element up with its context.
into :: [a] -> [ZL a]
into xs = moreInto $ LinLZ [] xs

moreInto :: LinLZ a -> [ZL a]
moreInto (LinLZ _ [])      = []
moreInto (LinLZ xs (y:ys)) = (ZL y (xs, ys)) : (moreInto $ LinLZ (y:xs) ys)

outward :: LinLZ a -> [ZL a]
outward (LinLZ [] _)      = []
outward (LinLZ (x:xs) ys) = (ZL x (xs, ys)) : (outward $ LinLZ xs (x:ys))

instance Comonad ZL where
    extract = this
    duplicate z@(ZL a (xs, ys)) =
        ZL z (outward (LinLZ xs (a:ys)), moreInto (LinLZ (a:xs) ys))

data TF t = Leaf
          | Fork (t, t)
    deriving (Show, Eq, Functor)

data BT a = a :& TF (BT a)
    deriving (Show, Eq, Functor)
