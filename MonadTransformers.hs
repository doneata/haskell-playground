{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

import Control.Monad.Identity
import Control.Monad.Reader
import Control.Monad.State
import Data.Map

type Name = String

type Value = Integer

type Env = Map Name Value

type Ptr = Value

data Expr = Lit Value
          | Expr :+: Expr
          | Var Name
          | Let Name Expr Expr
          | NewRef Expr
          | Deref Expr
          | Expr := Expr

data Store = Store { nextPtr :: Ptr, heap :: Map Ptr Value }

newtype Eval a = MkEval (StateT Store (ReaderT Env Identity) a)
    deriving (Functor, Applicative, Monad, MonadReader Env, MonadState Store)

emptyEnv :: Env
emptyEnv = empty

emptyStore :: Store
emptyStore = Store 0 empty

lookupVar :: Name -> Eval Value
lookupVar name = do
    env <- ask
    case Data.Map.lookup name env of
        Nothing  -> fail $ "Variable " ++ name ++ " not found"
        Just val -> return val

localScope :: Name -> Value -> Eval a -> Eval a
localScope name val = local (insert name val)

newRef :: Value -> Eval Ptr
newRef val = undefined
{-
eval :: Expr -> Eval Value
eval = \case
    Lit n          -> return n
    e1 :+: e2      -> (+) <$> eval e1 <*> eval e2
    Var name       -> lookupVar name
    Let name e1 e2 -> do
        v1 <- eval e1
        localScope name v1 (eval e2)

runEval :: Eval a -> a
runEval (MkEval reader) = runIdentity (runReaderT reader emptyEnv)
-}
