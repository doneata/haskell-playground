{- Working through the second assignment from Chalmers's course on Advanced
 - Functional Programming: http://www.cse.chalmers.se/edu/course/afp/lab2.html
 -}
{-# LANGUAGE InstanceSigs #-}

import Data.Time

data Instruction q r = Question q | Action (IO r)

-- data Replay q r a = Instructions [Instruction q r] | Finish a
--
data Replay q r a = Finish (IO a)

unFinish :: Replay q r a -> IO a
unFinish (Finish a) = a

instance Functor (Replay q r) where
    fmap :: (a -> b) -> Replay q r a -> Replay q r b
    fmap f (Finish a) = Finish (fmap f a)

instance Applicative (Replay q r) where
    pure :: a -> Replay q r a
    pure a = Finish (pure a)
    (<*>) :: Replay q r (a -> b) -> Replay q r a -> Replay q r b
    (Finish f) <*> (Finish a) = Finish (f <*> a)

instance Monad (Replay q r) where
    (>>=) :: Replay q r a -> (a -> Replay q r b) -> Replay q r b
    (Finish a) >>= f = Finish $ a >>= (unFinish . f)

type Trace r = [Item r]

data Item r = Answer r | String
    deriving (Show, Read)

emptyTrace :: Trace r
emptyTrace = []

addAnswer :: Trace r -> r -> Trace r
addAnswer t r = t ++ [Answer r]

io :: (Show a, Read a) => IO a -> Replay q r a
io = Finish

ask :: q -> Replay q r r
ask q = undefined

run :: Replay q r a -> Trace r -> IO (Either (q, Trace r) a)
run (Finish a) _ = undefined

example :: Replay String String Int
example = do
    t0 <- io getCurrentTime
    io (putStrLn "Hello!")
    age <- ask "What is your age?"
    io (putStrLn ("You are " ++ age))
    name <- ask "What is your name?"
    io (putStrLn (name ++ " is " ++ age ++ " years old"))
    t1 <- io getCurrentTime
    io (putStrLn ("Total time: " ++ show (diffUTCTime t1 t0)))
    return (read age)

example2 :: Replay String String Int
example2 = io (putStrLn "Hello") >> io (putStrLn "world") >> return 10

-- example2 = do
--     io (putStrLn "Hello world")
--     return 10
--

data Identity a = I a

instance Functor Identity where
    fmap f (I a) = I (f a)

instance Applicative Identity where
    pure a = I a
    (I f) <*> (I a) = I (f a)

instance Monad Identity where
    (I a) >>= f = f a
