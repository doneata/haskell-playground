{- Going through Gibbons's paper, "Origami programming":
 - https://www.cs.ox.ac.uk/jeremy.gibbons/publications/origami.pdf -}

import Data.Char (digitToInt)

-- §3.2 Origami with lists: sorting

data List a = Nil | Cons a (List a)
    deriving (Show, Eq)

xs :: List Int
xs = Cons 4 (Cons 1 (Cons 3 (Cons 2 Nil)))

wrap :: a -> List a
wrap a = Cons a Nil

nil :: List a -> Bool
nil Nil = True
nil _   = False

-- Fold for lists
foldL :: (a -> b -> b) -> b -> List a -> b
foldL f e Nil         = e
foldL f e (Cons x xs) = f x (foldL f e xs)

-- Exercise 3.1
-- Prove the fusion law:
--
--  (h (f a b) = f' a (h b)) ^ (h e = e')
-- =>
--  h . foldL f e = foldL f' e'
--
-- Proof. We prove the fusion law using structural induction.
--
-- Case Nil
--
--  (h . foldL f e) Nil
-- = { composition }
--  h (foldL f e Nil)
-- = { foldL.1 }
--  h e
-- = { assumption.2 }
--  e'
-- = { foldL.1 }
--  foldL f e' Nil
--
-- Case (Cons x xs)
--
--  (h . foldL f e) (Cons x xs)
-- = { composition }
--  h (foldL f e (Cons x xs))
-- = { foldL.2 }
--  h (f x (foldL f e xs))
-- = { assumption.1 }
--  f' x (h (foldL f e xs))
-- = { composition }
--  f' x ((h . foldL f e) xs)
-- = { induction }
--  f' x (foldL f' e' xs)
-- = { foldL.2 }
--  foldL f' e' (Cons x xs)
--
-- QED

-- Exercise 3.2
mapL :: (a -> b) -> List a -> List b
mapL f = foldL (Cons . f) Nil

appendL :: List a -> List a -> List a
appendL = flip $ foldL Cons

concatL :: List (List a) -> List a
concatL = foldL appendL Nil

-- Exercise 3.3
-- Prove the map fusion law:
--
--  foldL f e . mapL g = foldL (f . g) e
--
-- Proof.
--
-- First we rewrite the equation to prove, by replacing the function `mapL`
-- with its definition.
--
--  foldL f e . foldL (Cons . g) Nil = foldL (f . g) e  -- Eq. 1
--
-- We notice that equation 1 has the form of the fusion law if we pick:
--
--  h <~ foldL f e
--  f <~ Cons . g
--  e <~ Nil
--  f' <~ f . g
--  e '<~ e
--
-- Equation 1 is true by the virtue of the fusion law if we can prove the
-- following two equaltities:
--
--  foldL f e Nil = e                                     -- Eq. 2
--  foldL f e ((Cons . g) a b) = (f . g) a (foldL f e b)  -- Eq. 3
--
-- Equation 2 is true by applying the first defition of foldL, foldL.1.
--
-- Equation 3 is more interesting:
--
--  foldL f e ((Cons . g) a b)
-- = { composition }
--  foldL f e (Cons (g a) b)
-- = { foldL.2 }
--  f (g a) (foldL f e b)
-- = { composition }
--  (f . g) a (foldL f e b)
--
-- QED

-- Exercise 3.4
isort :: Ord a => List a -> List a
isort = foldL insert Nil

insert :: Ord a => a -> List a -> List a
insert y Nil         = wrap y
insert y (Cons x xs)
    | y < x          = Cons y (Cons x xs)
    | otherwise      = Cons x (insert y xs)

-- Definition of insert1 in terms of insert:
-- > insert1 y xs = (xs, insert y xs)
insert1 :: Ord a => a -> List a -> (List a, List a)
insert1 y = foldL f (Nil, wrap y)
    where f x (xs, acc) | y < x     = (Cons x xs, Cons y (Cons x xs))
                        | otherwise = (Cons x xs, Cons x acc)

-- Exercise 3.5
paraL :: (a -> (List a, b) -> b) -> b -> List a -> b
paraL f e Nil         = e
paraL f e (Cons x xs) = f x (xs, paraL f e xs)

insert2 :: Ord a => a -> List a -> List a
insert2 y = paraL f (wrap y)
    where f x (xs, acc) | y < x     = Cons y (Cons x xs)
                        | otherwise = Cons x (insert y xs)

-- Unfolds for Lists
unfoldL' :: (b -> Maybe (a, b)) -> b -> List a
unfoldL' f u = case f u of
                 Nothing     -> Nil
                 Just (x, v) -> Cons x (unfoldL' f v)

unfoldL :: (b -> Bool) -> (b -> a) -> (b -> b) -> b -> List a
unfoldL p f g b = case p b of
                    True  -> Nil
                    False -> Cons (f b) (unfoldL p f g (g b))

-- Exercise 3.6
-- (a) unfoldL' in terms of unfoldL
unfoldL'1 :: (b -> Maybe (a, b)) -> b -> List a
unfoldL'1 h u = unfoldL p f g u
    where
        p b = case h b of
                Nothing -> True
                Just _  -> False
        f b = case h b of
                Nothing     -> undefined
                Just (a, _) -> a
        g b = case h b of
                Nothing     -> undefined
                Just (_, b) -> b

-- (b) unfoldL in terms of unfoldL'
unfoldL1 :: (b -> Bool) -> (b -> a) -> (b -> b) -> b -> List a
unfoldL1 p f g b = unfoldL' h b
    where
        h u = case p u of
                True  -> Nothing
                False -> Just (f u, g u)

-- Exercise 3.7
-- Prove the unfold fusion law:
--
--  (p . h = p') ^ (f . h = f') ^ (g . h = h . g')
-- =>
--  unfoldL p f g . h = unfoldL p' f' g'
--
-- Proof. Taken from http://www.cs.ox.ac.uk/jeremy.gibbons/publications/corecursive.pdf
--
--  unfoldL p f g . h = unfoldL p' f' g'
-- <=> { universal property }
--  ∀ b . (unfoldL p f g . h) b = if p' b then Nil else Cons (f' b) ((unfoldL p f g . h) (g' b))
-- <=> { composition }
--  ∀ b . unfoldL p f g (h b) = if p' b then Nil else Cons (f' b) (unfoldL p f g (h (g' b)))
-- <=> { unfoldL }
--  ∀ b . if p (h b) then Nil else Cons (p (h b)) (unfoldL p f g (g (h b))) = if p' b then Nil else Cons (f' b) (unfoldL p f g (h (g' b)))
-- <=> { composition }
--  ∀ b . if (p . h) b then Nil else Cons (p . h) b (unfoldL p f g ((g . h) b)) = if p' b then Nil else Cons (f' b) (unfoldL p f g ((h . g') b))
-- <=  { assumptions }
--  (p . h = p') ^ (f . h = f') ^ (g . h = h . g')

-- Exercise 3.8
foldL' :: (Maybe (a, b) -> b) -> List a -> b
foldL' f Nil         = f Nothing
foldL' f (Cons x xs) = f (Just (x, foldL' f xs))

-- (a) foldL' in terms of foldL
foldL'1 :: (Maybe (a, b) -> b) -> List a -> b
foldL'1 f = foldL (\ a b -> f $ Just (a, b)) (f Nothing)

-- (b) foldL in terms of foldL'
foldL1 :: (a -> b -> b) -> b -> List a -> b
foldL1 f e = foldL' f'
    where
        f' Nothing       = e
        f' (Just (a, b)) = f a b

-- Exercise 3.9
foldLargs :: (a -> b -> b) -> b -> (Maybe (a, b) -> b)
foldLargs f e Nothing       = e
foldLargs f e (Just (a, b)) = f a b

unfoldLargs :: (b -> Bool) -> (b -> a) -> (b -> b) -> (b -> Maybe (a, b))
unfoldLargs p f g b = case p b of
    True  -> Nothing
    False -> Just (f b, g b)

-- Selection sort
delmin :: Ord a => List a -> Maybe (a, List a)
delmin Nil = Nothing
delmin xs  = Just (y, deleteL y xs)
    where
        y = minimumL xs

minimumL :: Ord a => List a -> a
minimumL (Cons x xs) = foldL min x xs

deleteL :: Ord a => a -> List a -> List a
deleteL y (Cons x xs) | x == y    = xs
                      | otherwise = Cons x (deleteL y xs)

ssort :: Ord a => List a -> List a
ssort = unfoldL' delmin

-- Exercise 3.10
deleteL' :: Ord a => a -> List a -> List a
deleteL' y = paraL (\ x (xs, acc) -> if x == y then xs else Cons x acc) Nil

-- Exercise 3.11
delmin' :: Ord a => List a -> Maybe (a, List a)
delmin' = paraL f Nothing
    where
        f x (xs, Nothing)      = Just (x, xs)
        f x (xs, Just (m, ms)) = if x < m
                                 then Just (x, xs)
                                 else Just (m, Cons x ms)

-- Bubble sort
bubble :: Ord a => List a -> Maybe (a, List a)
bubble = foldL step Nothing
    where
        step x Nothing        = Just (x, Nil)
        step x (Just (y, ys))
            | x < y           = Just (x, Cons y ys)
            | otherwise       = Just (y, Cons x ys)

bsort :: Ord a => List a -> List a
bsort = unfoldL' bubble

-- Exercise 3.12
bubble' :: Ord a => List a -> List a
bubble' = foldL step Nil
    where
        step x Nil            = wrap x
        step x (Cons y ys)
            | x < y           = Cons x (Cons y ys)
            | otherwise       = Cons y (Cons x ys)

-- Exercise 3.13
insert3 :: Ord a => a -> List a -> List a
insert3 y xs = unfoldL' f (xs, Just y)
    where
        f (Nil      , Nothing) = Nothing
        f (Nil      , Just y)  = Just (y, (Nil, Nothing))
        f (Cons x xs, Nothing) = Just (x, (xs, Nothing))
        f (Cons x xs, Just y)
            | x < y            = Just (x, (xs, Just y))
            | otherwise        = Just (y, (Cons x xs, Nothing))

-- Exercise 3.14
apoL' :: (b -> Maybe (a, Either b (List a))) -> b -> List a
apoL' f u = case f u of
    Nothing            -> Nil
    Just (x, Left v)   -> Cons x (apoL' f v)
    Just (x, Right xs) -> Cons x xs

insert4 :: Ord a => a -> List a -> List a
insert4 y xs = apoL' f (xs, Just y)
    where
        f (Nil      , Nothing) = Nothing
        f (Nil      , Just y)  = Just (y, Left (Nil, Nothing))
        f (Cons x xs, Nothing) = Just (x, Right xs)
        f (Cons x xs, Just y)
            | x < y            = Just (x, Left (xs, Just y))
            | otherwise        = Just (y, Left (Cons x xs, Nothing))

-- Hylomorphisms
hyloL f e p g h = foldL f e . unfoldL p g h

-- Exercise 3.15
-- Example:
-- > stringToBinary $ Cons '1' (Cons '6' Nil)
stringToBinary :: List Char -> List Bool
stringToBinary = intToBinary . stringToInt

stringToInt :: List Char -> Int
stringToInt = fst . foldL f (0, 0)
    where
        f c (n, p) = (n + round((fromIntegral $ digitToInt c) * 10 ** p), p + 1)

intToBinary :: Int -> List Bool
intToBinary = unfoldL (== 0) ((== 1) . (`mod` 2)) (`div` 2)

-- §3.3 Origami by numbers: loops

data Nat = Zero | Succ Nat
    deriving (Show, Eq)

-- Folds for naturals
foldN :: a -> (a -> a) -> Nat -> a
foldN z _ Zero     = z
foldN z f (Succ n) = f $ foldN z f n

fromNat :: Nat -> Int
fromNat = foldN 0 (+1)

toNat :: Int -> Nat
toNat 0 = Zero
toNat n = Succ $ toNat $ n - 1

-- The function `iter` is also known as the Church numeral of n
iter :: Nat -> (a -> a) -> (a -> a)
iter n f x = foldN x f n

-- Exercise 3.16
foldN' :: (Maybe a -> a) -> Nat -> a
foldN' f Zero     = f Nothing
foldN' f (Succ n) = f (Just (foldN' f n))

-- foldN' in terms of foldN
foldN'1 :: (Maybe a -> a) -> Nat -> a
foldN'1 f = foldN z g
    where
        z = f Nothing
        g = f . Just

-- foldN in terms of foldN'
foldN1 :: a -> (a -> a) -> Nat -> a
foldN1 z f = foldN' g
    where
        g Nothing  = z
        g (Just x) = f x

-- Exercise 3.17
-- (a) The universal property of foldN:
--  h = foldN z f
-- <=>
--  h n = case n of
--          Zero   -> z
--          Succ m -> f (h m)
--
-- (b) The fusion law for foldN:
--  h . foldN z f = foldN z' f'
-- <=
--  (h . f = f' . h) ∧ (h z = z')

-- Exercise 3.18
addN :: Nat -> Nat -> Nat
addN m n = foldN m Succ n

mulN :: Nat -> Nat -> Nat
mulN m n = foldN Zero (addN m) n

powN :: Nat -> Nat -> Nat
powN m n = foldN (Succ Zero) (mulN m) n

-- Exercise 3.19
predN :: Nat -> Maybe Nat
predN Zero     = Nothing
predN (Succ n) = Just n

predN' :: Nat -> Maybe Nat
predN' = foldN Nothing f
    where
        f Nothing  = Just Zero
        f (Just x) = Just (Succ x)

-- We can generalize the above functions by using *primitive recursion*. This
-- type of recursion is similar to iteration, but additionally provides access
-- to the iteration number to the iterator f. In other words, primitive
-- recursion abstracts the following recursive pattern:
--
--  h Zero     = z
--  h (Succ n) = f n (h n)
-- ↔
--  h = primRec z f
--
-- For more information, see §5 from the following lecture notes by Frank
-- Pfenning:
-- http://www.cs.cmu.edu/~fp/courses/15814-f21/lectures/02-primrec.pdf
primRec :: a -> (Nat -> a -> a) -> Nat -> a
primRec z f = snd . foldN z' f'
    where
        z'        = (Zero, z)
        f' (n, x) = (Succ n, f n x)

predN'' :: Nat -> Maybe Nat
predN'' = primRec Nothing (\n _ -> Just n)

-- Exercise 3.20
subN :: Nat -> Nat -> Maybe Nat
subN m n = foldN (Just m) f n
    where
        f Nothing  = Nothing
        f (Just x) = predN x

eqN :: Nat -> Nat -> Bool
eqN m n = case subN m n of
    Just Zero -> True
    _         -> False

lessN :: Nat -> Nat -> Bool
lessN m n = case subN m n of
    Nothing -> True
    _       -> False

-- Unfolds for naturals
unfoldN :: (a -> Bool) -> (a -> a) -> a -> Nat
unfoldN p f x = if p x then Zero else Succ (unfoldN p f (f x))

unfoldN' :: (a -> Maybe a) -> a -> Nat
unfoldN' f x = case f x of
    Nothing -> Zero
    Just y  -> Succ (unfoldN' f y)

-- Exercise 3.21
-- (a) unfoldN' in terms of unfoldN
unfoldN'1 f = unfoldN p g
    where
        p x = case f x of
            Nothing -> True
            Just _  -> False
        g x = case f x of
            Nothing -> undefined
            Just x  -> x

-- (b) unfoldN in terms of unfoldN'
unfoldN1 p f = unfoldN' g
    where
        g x = case p x of
            True  -> Nothing
            False -> Just (f x)

-- Exercise 3.22
-- (a) The universal property of unfoldN:
--  h = unfoldN p f
-- <=>
--  h b = if p b then Zero else Succ (h (f b))
--
-- (b) The fusion law for foldN:
--  unfoldN p f . h = unfoldL p' f'
-- <=
--  (p . h = p') ^ (f . h = f')

-- Exercise 3.23
divN :: Nat -> Nat -> Nat
divN m n = unfoldN' (\ x -> subN x n) m

-- Exercise 3.24
logN :: Nat -> Nat
logN = unfoldN (\ x -> lessN x base) (\ x -> divN x base)
    where
        base = toNat 2

-- Exercise 3.25
hyloN' :: (Maybe a -> a) -> (a -> Maybe a) -> a -> a
hyloN' f g = foldN' f . unfoldN' g

hyloN'1 :: (Maybe a -> a) -> (a -> Maybe a) -> a -> a
hyloN'1 f g n = case g n of
    Nothing -> f Nothing
    Just y  -> f $ Just $ hyloN'1 f g y

-- ? Exercise 3.26
untilN2 :: (a -> Bool) -> (a -> a) -> a -> a -> a
untilN2 p f x y = foldN x f (unfoldN p f y)

-- Proof.
--  untilN2 p f x y = foldN x f (unfoldN p f y)
-- <=> { unfoldN }
--  untilN2 p f x y = foldN x f (if p y then Zero else Succ (unfoldN p f (f y)))
-- <=> { foldN }
--  untilN2 p f x y = case (if p y then Zero else Succ (unfoldN p f (f y))) of
--                      Zero   -> x
--                      Succ n -> f (foldN x f n)
-- <=> { ??? }
--  untilN2 p f x y = if p y then x else f (foldN x f (unfoldN p f (f y)))
-- <=> { untilN2 }
--  untilN2 p f x y = if p y then x else f (untilN2 p f x (f y))

-- ? Exercise 3.27

-- Exercise 3.28
fix' :: (a -> a) -> a
fix' = hyloL ($) undefined (const True) id id

-- §3.4 Origami with trees: traversals

data Rose a   = Node a (Forest a) deriving (Eq, Show)
type Forest a = List (Rose a)

tree :: Rose Int
tree = Node 0
        (Cons (Node 1
          (Cons (Node 4 Nil) Nil))
        (Cons (Node 2 Nil)
        (Cons (Node 3 Nil) Nil)))

-- Folds for trees and forests

foldR :: (a -> c -> b) -> (List b -> c) -> Rose a -> b
foldR f g (Node a ts) = f a (foldF f g ts)

foldF :: (a -> c -> b) -> (List b -> c) -> Forest a -> c
foldF f g ts = g (mapL (foldR f g) ts)

-- Exercise 3.29
foldRose :: (a -> List b -> b) -> Rose a -> b
foldRose f (Node a ts) = f a (mapL (foldRose f) ts)

-- (a) foldRose in terms of foldR and foldF
foldRose' :: (a -> List b -> b) -> Rose a -> b
foldRose' f = foldR f id

-- (b) foldR and foldF in terms of foldRose
foldR' :: (a -> c -> b) -> (List b -> c) -> Rose a -> b
foldR' f g = foldRose (\ a ts -> f a (g ts))

foldF' :: (a -> c -> b) -> (List b -> c) -> Forest a -> c
foldF' f g = g . mapL (foldRose (\ a ts -> f a (g ts)))

-- Exercise 3.30
-- (a) Universal property
--  hR = foldR f g
--  hF = foldF f g
-- <=>
--  hR r  = case r of Node a ts -> f a (hF ts)
--  hF ts = g (mapL hR ts)
--
-- (b) Fusion law
--  h1 . foldR f g = foldR f' g'
--  h2 . foldF f g = foldF f' g'
-- <=
--  h1 . f a = f a . h2
--  h2 . g   = g . mapL h1
--
-- Proof.
--  (h1 . foldR f g) r  = foldR f' g' r
--  (h2 . foldF f g) ts = foldF f' g' ts
-- <=> { universal property }
--  (h1 . foldR f g) r  = case r of Node a ts -> f a ((h2 . foldF f g) ts)
--  (h2 . foldF f g) ts = g (mapL (h1 . foldR f g) ts)
-- <=> { composition }
--  h1 (foldR f g r)  = case r of Node a ts -> f a (h2 (foldF f g ts))
--  h2 (foldF f g ts) = g (mapL (h1 . foldR f g) ts)
-- <=> { foldR, foldF }
--  h1 (case r of Node a ts -> f a (foldF f g ts)) = case r of Node a ts -> f a (h2 (foldF f g ts))
--  h2 (g (mapL (foldR f g) ts)) = g (mapL (h1 . foldR f g) ts)
-- <=> { apply h1 function to cases }
--  case r of Node a ts -> h1 (f a (foldF f g ts)) = case r of Node a ts -> f a (h2 (foldF f g ts))
--  h2 (g (mapL (foldR f g) ts)) = g (mapL (h1 . foldR f g) ts)
-- <=> { map fusion: mapL (f . g) = mapL f . mapL g }
--  h1 (f a (foldF f g ts))  = f a (h2 (foldF f g ts))
--  h2 (g (mapL (foldR f g) ts)) = g (mapL h1 (mapL (foldR f g) ts))
-- <=> { composition }
--  (h1 . f a) (foldF f g ts)  = (f a . h2) (foldF f g ts)
--  (h2 . g) (mapL (foldR f g) ts) = (g . mapL h1) (mapL (foldR f g) ts)
-- <=  { assumptions }

-- Unfolds for trees and forests

-- Depth-first traversal
dft :: Rose a -> List a
dff :: Forest a -> List a
(dft, dff) = (foldR f g, foldF f g)
    where
        f = Cons
        g = concatL

-- Exercise 3.31
-- dff
-- = { dff }
-- foldF Cons concatL
-- = { foldF }
-- concatL . mapL (foldR Cons concatL)
-- = { concatL }
-- foldL appendL Nil . mapL (foldR Cons concatL)
-- = { map fusion }
-- foldL (appendL . foldR Cons concatL) Nil
-- = { lambda abstraction }
-- foldL (\ r xs -> appendL (foldR Cons concatL r) xs) Nil
-- = { appendL }
-- foldL (\ r xs -> foldL Cons xs (foldR Cons concatL r)) Nil
-- = { foldR }
-- foldL (\ r xs -> foldL Cons xs (case r of Node a ts -> Cons a (foldF Cons concatL ts))) Nil
-- = { dff }
-- foldL (\ r xs -> foldL Cons xs (case r of Node a ts -> Cons a (dff ts))) Nil
-- = { pattern matching }
-- foldL (\ (Node a ts) xs -> foldL Cons xs (Cons a (dff ts))) Nil
