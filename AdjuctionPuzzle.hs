-- Based on Matthew Doty's puzzle:
-- https://forum.azimuthproject.org/discussion/comment/16072/#Comment_16072

{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE Rank2Types             #-}
{-# LANGUAGE TypeOperators          #-}

module Adjunction where
import GHC.Generics

class (Functor f, Functor g) => Adjunction f g | f -> g, g -> f where
    leftAdjunct  :: (f a -> b) -> a -> g b
    rightAdjunct :: (a -> g b) -> f a -> b
    leftAdjunct  g a  = fmap g $ unit a
    rightAdjunct f fa = counit $ fmap f fa
    unit   :: a -> g (f a) 
    counit :: f (g a) -> a
    unit   = leftAdjunct id
    counit = rightAdjunct id
    {-# MINIMAL (unit, counit) | (leftAdjunct, rightAdjunct) #-}

instance Adjunction ((,) e) ((->) e) where
    leftAdjunct g a e = g (e, a)
    rightAdjunct f (e, a) = (f a) e

instance Adjunction f g => Applicative (g :.: f) where
    pure = undefined
    (<*>) = undefined

instance Adjunction f g => Monad (g :.: f) where
    (>>=) = undefined
