This repository contains bits and pieces of Haskell code written for my own amusement.

# Table of contents

- `15-814/`: exercises from CMU's excellent [Types and Programming Languages](http://www.cs.cmu.edu/~fp/courses/15814-f21) course
- `Adverbial.hs`: [Conor McBride's show](https://personal.cis.strath.ac.uk/conor.mcbride/Eugene/Conor/Lec1Slides/index.html) on how to use third-order functions (such as `ala`) to abstract over newtypes.
- `AlgebraOfProgramming.hs`: attempting some exercises from Bird and de Moor's book [Algebra of Programming](https://www.cs.ox.ac.uk/publications/books/algebra/).
- `AlternativeExercise.hs`: a first iteration on Oleg Grenrus [Alternative exercise](http://oleg.fi/gists/posts/2017-06-16-alternative-exercises.html)
- `ApplicativeRecorder.hs`: Ionuț's play in Applicative design.
- `BinomialHeaps.hs`: implementation of Ralf Hinze's paper ["Explaining binomial heaps"](https://dl.acm.org/doi/10.1017/S0956796899003317).
- `CS410-2010/`: Conor McBride's [course on functional programming](https://personal.cis.strath.ac.uk/conor.mcbride/CS410/).
- `CS410-2010/Prac5.hs`: using the teletype free monad to loop Hutton's chatterboxes.
- `CS410-2010/Prac6.hs`: an exploration of "polynomial" functors built in kit form.
- `CalculatingFunctionalPrograms.hs`: implementing Jeremy Gibbons' [paper](http://www.cs.ox.ac.uk/people/jeremy.gibbons/publications/acmmpc-calcfp.pdf).
- `ComonadsAreObjects.hs`: Gabriel Gonzalez's [tutorial on comonads](http://www.haskellforall.com/2013/02/you-could-have-invented-comonads.html).
- `ComonadsCellularAutomata.hs`: Dan Piponi's [blog-post on comonads and cellurar automata](http://blog.sigfpe.com/2006/12/evaluating-cellular-automata-is.html).
- `ComonadsGameOfLife.hs`: Conway's Game of Life implemented using the Store comonad.
- `ContinuationPassingStyle.hs`: a timid incursion on the CPS.
- `DataTypesALaCarte.hs`: based on W. Swierstra's [paper](http://www.cs.ru.nl/~W.Swierstra/Publications/DataTypesALaCarte.pdf) and on the [CodeWars problem](https://www.codewars.com/kata/data-types-a-la-carte-fork/train/haskell).
- `DesignCoprograms.hs`: dissecting Jeremy Gibbons' post ["How to design co-programs"](https://patternsinfp.wordpress.com/2018/11/21/how-to-design-co-programs/).
- `DifferenceList.hs`: learning about difference lists, or [Cayley's representation for the list monoid](https://arxiv.org/abs/1406.4823).
- `FreeMonoidCoproduct.hs`: showing that the free monoid preserves coproducts.
- `FreeMonoids.hs`: toying with use cases of lists being the free monoid.
- `FunctionalImages.hs`: [The Fun of Programming](https://www.cs.ox.ac.uk/publications/books/fop/) [§7 Functional Images](conal.net/papers/functional-images/fop-conal.pdf).
- `FoldEDSL.hs`: implementing [Folding domain-specific languages](https://dl.acm.org/doi/10.1145/2628136.2628138) by Jeremy Gibbons and Nicholas Wu.
- `FreeMonads.hs`: playing with the teletype data type.
- `IOAction.hs` and `IOActionFreeMonad.hs`: my first foray into free monads.
- `LogoFreeMonad.hs`: interpreters for the Logo language (based on a tutorial in Scala).
- `MonadicParsing.hs`: learning about Graham Hutton's approach to parsing.
- `MonoidsToApplicative.hs`: an attempt in generating `Applicative` functors based on various `Monoids` on `List`s.
- `Origami.hs`: [The Fun of Programming](https://www.cs.ox.ac.uk/publications/books/fop/) [§3 Origami Programming](https://www.cs.ox.ac.uk/jeremy.gibbons/publications/origami.pdf).
- `ParameterisedMonads.hs`: an implementation and demo of the parameterised state monad (based on Jeremy Yallop's OCaml [implementation](https://www.cl.cam.ac.uk/teaching/1415/L28/monads.pdf)).
- `SmoothPerm.hs`: an exercise in data representation and tree unfolding; from the first assignment of the [Advanced Functional Programming](http://www.cs.uu.nl/docs/vakken/afp/) course in Utrecht, the 2021 edition.
- `StreamMonad.hs`: playing with the [stream monad](https://patternsinfp.wordpress.com/2010/12/31/stream-monad/).
- `Sudoku.hs`: [Conor McBride](strictlypositive.org/)'s Sudoku-flavoured [exercises](https://stackoverflow.com/a/10242673/474311) on Applicative and Traversable.
- `Travesable.hs`: revisiting the `Traversable` typeclass—an investigation based on the [Typeclassopedia](https://wiki.haskell.org/Typeclassopedia#Traversable).
- `UU-AFP-2019/`: exercises from the summer school on Advanced Functional Programming.
- `UU-AFP-2019/GamblingMonad.hs`: estimating the probability of events using the `RandomIO` and `DecisionTree` monads.
- `UU-AFP-2019/InstrumentedStateMonad.hs`: counting the number of `bind`s, `return`s, `get`s and `put`s when using a `MonadState` instance.
While instructive this exercise has some issues—[the `Monad` instance seems to violate the laws](https://stackoverflow.com/a/54281715/474311).
- `ZippersWikibooks.hs`: from [the Haskell wikibook](https://en.wikibooks.org/wiki/Haskell/Zippers).

Unfinished business:

- `AdjunctionPuzzle.hs`
- `ReplayMonad.hs`
- `State.hs`

# Further references

- Dan Piponi on [co-algebras](https://dl.dropboxusercontent.com/u/828035/Computing/fold.html)

# Notes

[On the comonad interface from ConorMcBride](https://stackoverflow.com/a/25530311/474311):

- `extract` means "give element here"
- `duplicate` means "decorate each element with its context"

---

[On the Store comonad from Russel O'Connor](https://stackoverflow.com/a/11179631/474311):

Given the following definition of store,

    data Store s a = Store { peek :: s -> a, pos :: s }

I like to think of a `Store` as a big warehouse filled with values of type `a`.
Each value of type `a` is slotted into a position labeled by an index value of type `s`.
Finally there is a forklift parked at position `pos`.
The forklift can be used to `extract` a value of type `a` from the store by pulling the value out from where it is parked.
You can use `seek` to move the forklift to a new absolute position or use `seeks` to move the forklift to a new relative location.
To update all values of the store use `fmap`.
Finally `extend f` is similar to `fmap` except instead of `f :: a -> a'` we have `f :: Store s a -> a'` which lets the update function not only have access to the value being updated but also gives access to the value's position and access to the values of everything else in the store.
In other words, `extend` uses the value plus its surrounding context to perform the update.

---

_Abstracting over functor shape._
The same functor can be represented in multiple ways by changing the constructors' name.
However, I would like to be able to reuse functions when functors have the same shape.
Let's take as an example the functor, `F(X) = 1 + X`;
it can be encoded in the following ways:

```haskell
data MyEither a   = Left () | Right a
data NatF     a   = ZeroF   | SuccF a
data ListF    a b = NilF    | ConsF a b  -- assuming we fix `a`
```

I would like the `cofork` functions to work on all those type,
without have to specify it for each instance separately:

```haskell
coforkMyEither :: ((() -> c), (a      -> c)) -> MyEither a   -> c
coforkNatF     :: ((() -> c), (a      -> c)) -> NatF     a   -> c
coforkListF    :: ((() -> c), ((a, b) -> c)) -> ListF    a b -> c
```

---
