{- Implementing a subset of the Logo language using free monads.
 - Based on the following tutorial in Scala:
 - https://blog.scalac.io/2016/06/02/overview-of-free-monad-in-cats.html
 -
 - See also, Jeremy Gibbons's paper, "Free Delivery":
 - http://www.cs.ox.ac.uk/jeremy.gibbons/publications/delivery.pdf
 -
 - TODO:
 - [ ] Implement interpreter to draw turtle and path
 -}

{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}

import Control.Monad.Free
import Data.Functor.Coyoneda
import Data.Functor.Identity

-- My first intention was to use a standard ADT to encode the syntax of the
-- language. Unfortunately, this approach doesn't work because of the
-- restriction of having a natural transformation from `Command a` to `m a`
-- (this is required by the `foldFree` function).
-- > data Command a = Forward      Position Integer
-- >                | Backward     Position Integer
-- >                | RotateLeft   Position Degree
-- >                | RotateRight  Position Degree
-- >                | ShowPosition Position
-- >     deriving (Eq, Show)
data Command a where
        Forward      :: Position -> Integer -> Command Position
        Backward     :: Position -> Integer -> Command Position
        RotateLeft   :: Position -> Degree  -> Command Position
        RotateRight  :: Position -> Degree  -> Command Position
        ShowPosition :: Position -> Command ()

data Position = Pos { unX :: Double, unY :: Double, unDeg :: Degree }
    deriving (Eq, Show)

type Degree = Integer

-- Comment from the "Free Delivery" paper; it motivates the need for co-Yoneda:
-- > The datatype `Command` has the right kind to be a functor, but because its
-- > type parameter is a phantom type, we cannot complete the definition—given
-- > a function `f` of type `r → r'`, there is no general way to define
-- > `fmap f :: Command r → Command r'`.
type Instruction = Free (Coyoneda Command)

effect :: Command a -> Instruction a
effect = liftF . liftCoyoneda

forward :: Position -> Integer -> Instruction Position

forward pos len = effect $ Forward pos len

backward :: Position -> Integer -> Instruction Position
backward pos len = effect $ Backward pos len

left :: Position -> Degree -> Instruction Position
left pos deg = effect $ RotateLeft pos deg

right :: Position -> Degree -> Instruction Position
right pos deg = effect $ RotateRight pos deg

showPos :: Position -> Instruction ()
showPos pos = effect $ ShowPosition pos

program :: Position -> Instruction Position
program start = do p1 <- forward start 10
                   p2 <- right p1 90
                   p3 <- forward p2 10
                   showPos p3
                   return p3

dx :: Integer -> Degree -> Double
dx u theta = fromInteger u * cos (fromInteger theta * pi / 180)

dy :: Integer -> Degree -> Double
dy u theta = fromInteger u * sin (fromInteger theta * pi / 180)

interpId :: Command a -> Identity a
interpId = \case
    Forward pos len ->
        let
            theta = unDeg pos
            x = unX pos + dx len theta
            y = unY pos + dy len theta
            pos'  = Pos x y theta
        in
            Identity pos'
    Backward pos len ->
        let
            theta = - unDeg pos
            x = unX pos + dx len theta
            y = unY pos + dy len theta
            pos'  = Pos x y theta
        in
            Identity pos'
    RotateLeft pos deg ->
        let
            theta = unDeg pos + deg
            pos'  = Pos (unX pos) (unY pos) theta
        in
            Identity pos'
    RotateRight pos deg ->
        let
            theta = unDeg pos - deg
            pos'  = Pos (unX pos) (unY pos) theta
        in
            Identity pos'
    ShowPosition pos -> Identity ()

run :: Monad m => (forall x. Command x -> m x) -> Instruction a -> m a
run interp = foldFree (lowerCoyoneda . hoistCoyoneda interp)

-- Example:
-- > run interpId (program (Pos 0 0 0))

-- -- TODO
-- import Graphics.Gloss
--
-- width  = 300
-- height = 300
-- offset = 100
--
-- window :: Display
-- window = InWindow "Logo" (width, height) (offset, offset)
--
-- background :: Color
-- background = black
--
-- main :: IO ()
-- main = display window background drawing
--
-- drawing :: Picture
-- drawing = circle 80
