{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}

-- | Adverbial programming
-- This file follows Conor McBride's presentation from Oregon Programming
-- Languages Summer School in 2010 and other answers from Stackoverflow.
-- https://www.cs.uoregon.edu/research/summerschool/summer10/curriculum.html

-- | The @newtype@ keyword provides a mechanism of making a differently named
-- copy of an old type. Intuitively, @newtype@ allows us to specify what "spin"
-- or structure on data we want to access.
--
-- Here is an example:
newtype F = F { unF :: Int }

-- However, we can give a @pack@ and @unpack@ method generally:
class Newtype n o | n -> o where
    pack   :: o -> n
    unpack :: n -> o

-- And define the unpack operation generally, without resorting to funny names
-- such as @unF@. Here the first argument of type o → n is needed just to
-- provide type information to the compiler to know which @unpack@ method to
-- use.
op :: Newtype n o => (o -> n) -> (n -> o)
op _ = unpack

data F' = F' Int

instance Newtype F' Int where
    pack          = F'
    unpack (F' x) = x

-- Example:
-- > op pack (F' 5)

-- McBride points out that @ala@ is an example of third order programming: it
-- is a function that transforms higher-order functions to work from structured
-- to unstructured data:
-- - @n@ and @n'@ are   structured types;
-- - @o@ and @o'@ are unstructured types.
ala :: (Newtype n o, Newtype n' o') =>
    (o -> n) -> ((a -> n) -> b -> n') -> ((a -> o) -> b -> o')
ala _ hof f = unpack . hof (pack . f)

-- Accumulation in some monoid is an effect that is not monadic, but
-- is applicative. Concretely, the constant type operator given the monoid is
-- applicative.
newtype Const a b = Const a

instance Functor (Const a) where
    fmap _ (Const a) = Const a

instance Monoid a => Applicative (Const a) where
    pure _               = Const mempty
    Const a <*> Const a' = Const (a <> a')

instance Newtype (Const a x) a where
    pack             = Const
    unpack (Const a) = a

-- McBride highlights that:
-- > foldMap = ala Const traverse
-- But how does this work?
-- ala       :: (o -> n) -> ((a -> n) -> b -> n') -> ((a -> o) -> b -> o')
-- Const     :: a -> Const a b
-- ala Const :: ((a1 -> Const a2 b1) -> b2  -> n'     ) -> (a1 -> a2) -> b2 -> o'
-- traverse  :: (a   -> f b)         -> t a -> f (t b)
-- traverse  ::  (a1 -> Const a2 b1) -> t a -> Const a (t b) 
-- ala Const traverse :: (a -> ???) -> t a -> o'

newtype Sum a = Sum a

instance Num a => Semigroup (Sum a) where
    Sum m <> Sum n = Sum (m + n)

instance Num a => Monoid (Sum a) where
    mempty = Sum (fromInteger 0)

instance Newtype (Sum a) a where
    pack           = Sum
    unpack (Sum a) = a

mySum :: (Foldable f, Num a) => f a -> a
mySum = ala Sum foldMap id

-- More examples, from McBride's "blook"
--
-- | Merge sort
-- 1. Use @foldr@ to split a list into odd and even positions.
splitList :: [a] -> ([a], [a])
splitList = foldr (\ a (xs, ys) -> (a:ys, xs)) ([], [])

-- 2. Introduce a "newtype" for the List named @Merge@.
data Merge a = Merge [a]
    deriving (Eq, Show)

instance Newtype (Merge a) [a] where
    pack              = Merge
    unpack (Merge xs) = xs

-- 3. Define Monoid instance for Merge
instance Ord a => Semigroup (Merge a) where
    xs           <> Merge []                 = xs
    Merge []     <> ys                       = ys
    Merge (x:xs) <> Merge (y:ys) | x <= y    = x `consM` (Merge xs <> Merge (y:ys))
                                 | otherwise = y `consM` (Merge (x:xs) <> Merge ys)

consM :: a -> Merge a -> Merge a
consM x (Merge ys) = Merge (x:ys)

instance Ord a => Monoid (Merge a) where
    mempty = Merge []

-- 4. We get insertion sort if we @ala Merge foldMap (:[])@
insertionSort :: Ord a => [a] -> [a]
insertionSort = ala Merge foldMap (:[])
-- > insertionSort [3, 1, 2, 5, 6, 0]

-- 5. Introduce an intermediate tree data structure of the form
-- T A = μ . 1 + A + - × -, where the unit, @None@ label, is used to initialize
-- the folding process.
data Tree a = None | One a | Node (Tree a) (Tree a)
    deriving (Eq, Show)

instance Foldable Tree where
    foldMap f None      = mempty
    foldMap f (One a)   = f a
    foldMap f (Node l r) = foldMap f l <> foldMap f r

-- 6. Use @foldr@ to build the tree (similarly to what we did for @splitList@;
-- then follow with @foldMap@ to consume it and build the sorted list.
twistin :: a -> Tree a -> Tree a
twistin a None      = One a
twistin a (One b)   = Node (One a) (One b)
twistin a (Node l r) = Node (twistin a r) l

mergeSort :: Ord a => [a] -> [a]
mergeSort = ala Merge foldMap (:[]) . foldr twistin None
-- > mergeSort [3, 1, 2, 5, 6, 0]

-- I think this is in the same spirit to Gibbons's example on structuring
-- co-recursive programs.
--
-- | Bunched accumulations
-- https://stackoverflow.com/questions/28701574/long-working-of-program-that-count-ints/28727176#28727176
data Bunched a = Bunched [a]
    deriving (Eq, Show)

instance Semigroup a => Semigroup (Bunched a) where
    Bunched [] <> Bunched ys = Bunched ys
    Bunched xs <> Bunched [] = Bunched xs
    Bunched (x:xs) <> Bunched (y:ys) = (x <> y) `consB` (Bunched xs <> Bunched ys)
        where consB x (Bunched xs) = Bunched (x:xs)

instance Monoid a => Monoid (Bunched a) where
    mempty = Bunched [mempty | i <- [0..]]

instance Newtype (Bunched a) [a] where
    pack                = Bunched
    unpack (Bunched xs) = xs

accumInts :: [Int] -> [[Int]]
accumInts = ala Bunched foldMap (basis !!)

basis :: [[[Int]]]
basis = zipWith (++) (iterate ([]:) []) [[[i]] | i <- [0..]]

-- I would have thought this also works, but it doesn't seem to be the case.
-- Not sure why laziness doesn't work here as expected.
-- basis :: Int -> [[Int]]
-- basis n = [if i == n then [i] else [] | i <- [0..]]
