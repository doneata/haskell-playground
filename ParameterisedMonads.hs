-- based on https://www.cl.cam.ac.uk/teaching/1415/L28/monads.pdf

-- TODO
-- [ ] use do notation

class ParametrisedMonad m where
    preturn :: a -> m s s a
    pbind :: m r s a -> (a -> m s t b) -> m r t b

class ParametrisedMonad m => ParametrisedState m where
    get :: m s s s
    put :: s -> m r s ()

data PState s t a = PState (s -> (t, a))

runPState :: PState s t a -> s -> (t, a)
runPState (PState f) s = f s

instance ParametrisedMonad PState where
    preturn a = PState $ \s -> (s, a)
    pbind p f = PState $ \r -> let (s, a) = runPState p r in runPState (f a) s

instance ParametrisedState PState where
    get   = PState $ \s -> (s, s)
    put s = PState $ \_ -> (s, ())

add :: PState (Int, (Int, s)) (Int, s) ()
add = get `pbind` \(x, (y, s)) ->
      put (x + y, s)

if' :: PState (Bool, (a, (a, s))) (a, s) ()
if' = get `pbind` \(cond, (t, (f, s))) ->
      put (if cond then t else f, s)

pushConst :: a -> PState s (a, s) ()
pushConst a = get `pbind` \s ->
              put (a, s)

program1 = pushConst 3 `pbind` \() ->
           pushConst 4 `pbind` \() ->
           add

program2 = pushConst 3 `pbind` \() ->
           pushConst 4 `pbind` \() ->
           pushConst 5 `pbind` \() ->
           pushConst True `pbind` \() ->
           if' `pbind` \() ->
           add

-- runPState program2 (10, (20, ()))
