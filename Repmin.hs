# The Essence of the Iterator Pattern
# http://ropas.snu.ac.kr/~bruno/papers/Iterator.pdf

import Data.Functor.Constant
import Data.Tree

newtype Min a = Min { unMin :: a }
    deriving Show

newtype Env e a = Env { unEnv :: e -> a }

instance (Ord a, Bounded a) => Monoid (Min a) where
    mempty                              = Min maxBound
    mappend (Min x) (Min y) | x < y     = Min x
                            | otherwise = Min y

t :: Tree Int
t = Node 17 [Node 23 [], Node 13 [Node 14 [], Node 53 []]]

tmin1 :: (Ord a, Bounded a) => a -> Constant (Min a) a
tmin1 = Constant . Min

trep1 :: a -> Env b b
trep1 _ = Env id

trepmin1 :: (Ord a, Bounded a) => Tree a -> Tree a
trepmin1 = undefined
