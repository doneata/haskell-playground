{-# LANGUAGE InstanceSigs #-}
-- Evaluating cellular automata is comonadic — Dan Piponi
-- http://blog.sigfpe.com/2006/12/evaluating-cellular-automata-is.html

data U a = U [a] a [a]

u = U [-1,-2..] 0 [1,2..]
v = U (repeat False) True (repeat False)

right :: U a -> U a
right (U xs a (y:ys')) = U (a:xs) y ys'

left :: U a -> U a
left (U (x:xs') a ys) = U xs' x (a:ys)

instance Functor U where
    fmap f (U xs a ys) = U (fmap f xs) (f a) (fmap f ys)

class Functor w => Comonad w where
    (=>>) :: w a -> (w a -> b) -> w b
    coreturn :: w a -> a
    cojoin :: w a -> w (w a)

    cojoin wa = (=>>) wa id
    wa =>> f = fmap f $ cojoin wa

instance Comonad U where
    coreturn :: U a -> a
    coreturn (U _ a _) = a

    cojoin :: U a -> U (U a)
    cojoin u = U (tail $ iterate left u) u (tail $ iterate right u)

rule :: U Bool -> Bool
rule (U (a:_) b (c:_)) = not (a && b && not c || (a == b))

-- Examples:
-- > coreturn $ v =>> rule
-- > coreturn $ right $ v =>> rule
-- > coreturn $ left $ v =>> rule

shift :: Int -> U a -> U a
shift n u | n > 0 = shift (n - 1) $ right u
          | n < 0 = shift (n + 1) $ left u
          | otherwise = u

toList :: Int -> Int -> U a -> [a]
toList i j u = let U _ a ys = shift i u in a:take (j - i) ys

toChar :: Bool -> Char
toChar True = '*'
toChar False = ' '

example :: IO ()
example = putStrLn
        $ unlines
        $ take 20
        $ fmap (fmap toChar . toList (-20) 20)
        $ iterate (=>> rule) v
