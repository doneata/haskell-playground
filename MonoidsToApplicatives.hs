{-# LANGUAGE DeriveFunctor #-}

-- This snippet attempts the following puzzle from Conor McBride (pigworker):
-- > Find at least four behaviourally distinct instances of Applicative Nellist
-- > which obey the applicative laws.
-- https://stackoverflow.com/a/32825891/474311
--
-- Alternatively, I regard this snippet as an exploration of the following
-- question:
-- > Does every monoid induce an applicative?
-- Every `Applicative` has an underlying `Monoid`. If the functor is a
-- container than the `Monoid` operates on the shape of the functor.
-- I'm wondering whether the converse is true.

class Functor f => Monoidal f where
    unit :: a -> f a
    fzip :: f a -> f b -> f (a, b)

    starAp :: f (a -> b) -> f a -> f b
    starAp h x = fmap (uncurry ($)) (fzip h x)

-- A `Nellist` is a non-empty list, with a doubled L to clarify the pronunciation.
data Nellist x = x :& Maybe (Nellist x)
    deriving (Show, Eq, Functor)

-- | Append two non-empty lists
(.++) :: Nellist a -> Nellist a -> Nellist a
(x :& Nothing) .++ ys = x :& Just ys
(x :& Just xs) .++ ys = x :& Just (xs .++ ys)

-- | List → Monoid (N, 1, *)
-- instance Monoidal Nellist where
--     unit = (:& Nothing)
--     fzip (a :& Nothing) (b :& Nothing) = (a, b) :& Nothing
--     fzip (a :& Just as) (b :& Nothing) = (a, b) :& Just (fzip as (unit b))
--     fzip (a :& Nothing) (b :& Just bs) = (a, b) :& Just (fzip (unit a) bs)
--     fzip (a :& Just as) (b :& Just bs) = (a, b) :& Just (fzip (unit a) bs .++ fzip as (unit b) .++ fzip as bs)

-- | ZipList → Monoid (N, infty, min)
-- instance Monoidal Nellist where
--     unit a = a :& Just (unit a)
--     fzip (a :& Just as) (b :& Just bs) = (a, b) :& Just (fzip as bs)
--     fzip (a :& _      ) (b :& _      ) = (a, b) :& Nothing

-- | PadMe → Monid ([1..], 1, max) (the last element is used for padding)
-- | Based on this example https://stackoverflow.com/a/21350096/474311
-- instance Monoidal Nellist where
--     unit = (:& Nothing)
--     fzip (a :& Nothing) (b :& Nothing) = (a, b) :& Nothing
--     fzip (a :& Just as) (b :& Nothing) = (a, b) :& Just (fzip as (unit b))
--     fzip (a :& Nothing) (b :& Just bs) = (a, b) :& Just (fzip (unit a) bs)
--     fzip (a :& Just as) (b :& Just bs) = (a, b) :& Just (fzip as bs)

-- | Mistery → Monid (N, 0, +)
-- | I think this is similar to this implementation
-- | https://stackoverflow.com/questions/50701827/why-doesnt-the-list-applicative-instance-perform-a-one-to-one-application?noredirect=1&lq=1#comment88497286_50726062
instance Monoidal Nellist where
    unit = (:& Nothing)
    fzip (a :& Nothing) (b :& Nothing) = (a, b) :& Nothing
    fzip (a :& Just as) (b :& Nothing) = (a, b) :& Just (fzip as (unit b))
    fzip (a :& Nothing) (b :& Just bs) = (a, b) :& Just (fzip (unit a) bs)
    fzip (a :& Just as) (b :& Just bs) = fzip (unit a) (b :& Just bs) .++ fzip (a :& Just as) (unit b)

as = 1 :& Just (2 :& Nothing)
bs = 3 :& Just (4 :& Just (5 :& Nothing))
cs = 6 :& Just (7 :& Just (8 :& Nothing))
