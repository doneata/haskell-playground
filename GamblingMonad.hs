-- An exericse from the summer school on Advanced Functional Programming
-- https://uu-afp.github.io/as1.html#monads-for-a-gambling-game
import System.Random

data Coin = H | T
data Dice = D1 | D2 | D3 | D4 | D5 | D6
data Outcome = Win | Lose

class Monad m => MonadGamble m where
    toss :: m Coin
    roll :: m Dice

isHead :: Coin -> Bool
isHead H = True
isHead T = False

diceVal :: Dice -> Int
diceVal D1 = 1
diceVal D2 = 2
diceVal D3 = 3
diceVal D4 = 4
diceVal D5 = 5
diceVal D6 = 6

-- Q Why not define the type as `MonadGable Outcome`?
game :: MonadGamble m => m Outcome
game = do
    toss' <- sequence $ (take 6) $ repeat toss
    roll' <- roll
    let h = length $ filter isHead toss'
    let d = diceVal roll'
    if d >= h then return Win else return Lose

instance Random Coin where
    random  g = undefined
    randomR g = undefined

