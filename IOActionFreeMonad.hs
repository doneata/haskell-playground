{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE LambdaCase #-}

import Control.Monad.Free
import Control.Monad.RWS.Lazy hiding (get, put)

-- Data type
data IOActionF a = Put String a
                 | Get (String -> a)
    deriving Functor

type IOAction = Free IOActionF

-- Utilities
get :: IOAction String
get = liftF $ Get id

put :: String -> IOAction ()
put s = liftF $ Put s ()

-- Sample programs
echo :: IOAction ()
echo = get >>= put

echo' :: IOAction ()
echo' =
    do word <- get
       if word == "\04"  -- Ctrl-D
       then return ()
       else put word >> echo'

hello :: IOAction ()
hello = put "What is your name?"      >>= \_    ->
        get                           >>= \name ->
        put "What is your age?"       >>= \_    ->
        get                           >>= \age  ->
        put ("Hello " ++ name ++ "!") >>= \_    -> 
        put ("You are " ++ age ++ " years old")

-- Interpreters: IO and RWS
interpIO :: IOAction a -> IO a
interpIO = foldFree i
    where
        i = \case
            Put s a -> putStrLn s >> return a
            Get f   -> getLine >>= return . f

type IOActionRWS = RWS [String] () [String]

-- See this answer on why we need `iterM` instead of `foldFree`:
-- https://stackoverflow.com/a/52883770/474311
interpRWS :: IOAction a -> IOActionRWS a
interpRWS = iterM i
    where 
        i = \case
            Put s a -> modify (\t -> t ++ [s]) >> a
            Get f   -> reader head >>= local tail . f

mockConsole :: IOAction a -> [String] -> (a, [String])
mockConsole p inp = (a, s)
    where
        (a, s, _) = runRWS (interpRWS p) inp []

-- Examples:
-- > inerpIO hello
-- > mockConsole hello ["john", "18"]
