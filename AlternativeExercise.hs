-- http://oleg.fi/gists/posts/2017-06-16-alternative-exercises.html

import Text.Regex.Applicative

exactCount :: Alternative f => Int -> f a -> f [a]
exactCount 0 _ = pure []
exactCount n v = pure (:) <*> v <*> exactCount (n - 1) v

upto :: Alternative f => Int -> f a -> f [a]
upto 0 _ = pure []
upto n v = exactCount n v <|> upto (n - 1) v

count' :: Alternative f => Int -> Int -> f a -> f [a]
count' n m v = pure (++) <*> exactCount n v <*> upto (m - n) v

-- Examples:
-- > "a" =~ count' 2 3 (sym 'a')
-- Nothing
-- > "aa" =~ count' 2 3 (sym 'a')
-- Just "aa"
-- > "aaa" =~ count' 2 3 (sym 'a')
-- Just "aaa"
-- > "aaaa" =~ count' 2 3 (sym 'a')
-- Nothing
-- > "ab" =~ count' 2 3 (sym 'a')
-- Nothing
