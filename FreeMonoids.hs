import Data.Monoid

emb :: a -> [a]
emb x = [x]

-- Specify how to behave on a individual elements → we can extraloate on
-- behaviour on the entire list.
-- The alternative would be recursion?
interp :: Monoid m => (a -> m) -> ([a] -> m)
interp i = mconcat . fmap i

-- Analogy with the Free monad:
-- > interp :: Monad m => (forall x. f x -> m x) -> (Free f a -> m a)

-- Example 1: Sum
sum :: [Int] -> Sum Int
sum = interp Sum

-- Example 2: Length
len :: [Int] -> Sum Int
len = interp $ const $ Sum 1

-- Example 3: Contains
contains :: Int -> [Int] -> Any
contains n = interp $ Any . (== n)
