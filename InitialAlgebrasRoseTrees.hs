{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE DeriveFunctor #-}

newtype K a   x = K a deriving Functor           -- constant functor
newtype P f g x = P (f x, g x) deriving Functor  -- products

newtype FixF f = InF (f (FixF f))

type Rose a = FixF (P (K a) [])

pattern Node :: a -> [Rose a] -> Rose a
pattern Node a ars = InF (P (K a, ars))
