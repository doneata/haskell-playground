{-# LANGUAGE InstanceSigs #-}

import Data.MemoTrie

data Store s a = Store (s -> a) s
-- Alternative, more suggestive definition:
-- > data Store s a = Store { peek :: s -> a, pos :: s }

instance HasTrie s => Functor (Store s) where
    fmap :: (a -> b) -> Store s a -> Store s b
    fmap f (Store g s) = Store (memo $ f . g) s

class Functor w => Comonad w where
    extend :: (w a -> b) -> w a -> w b
    extract :: w a -> a
    duplicate :: w a -> w (w a)

    duplicate = extend id
    extend f = fmap f . duplicate

instance HasTrie s => Comonad (Store s) where
    extract :: Store s a -> a
    extract (Store f a) = f a

    duplicate :: Store s a -> Store s (Store s a)
    duplicate (Store f s) = Store (Store f) s

type Pos = (Int, Int)

seed :: Store Pos Bool
seed = Store g (0, 0)
    where
        g ( 0,  1) = True
        g ( 1,  0) = True
        g (-1, -1) = True
        g (-1,  0) = True
        g (-1,  1) = True
        g _        = False

neighbours8 :: [Pos]
neighbours8 = [(-1,  1), (0,  1), (1,  1),
               (-1,  0),          (1,  0),
               (-1, -1), (0, -1), (1, -1)]

move :: Store Pos a -> Pos -> Store Pos a
move (Store f (x, y)) (dx, dy) = Store f (x + dx, y + dy)

count :: [Bool] -> Int
count = length . filter id

getNrAliveNeighs :: Store Pos Bool -> Int
getNrAliveNeighs s = count $ fmap (extract . move s) neighbours8

rule :: Store Pos Bool -> Bool
rule s = let n = getNrAliveNeighs s
         in case (extract s) of
            True  -> 2 <= n && n <= 3
            False -> n == 3

blockToStr :: [[Bool]] -> String
blockToStr = unlines . fmap (fmap f)
    where
        f True  = '*'
        f False = '.'

getBlock :: Int -> Store Pos a -> [[a]]
getBlock n store@(Store _ (x, y)) =
    [[extract (move store (dx, dy)) | dy <- yrange] | dx <- xrange]
    where
        yrange = [(x - n)..(y + n)]
        xrange = reverse yrange

example :: IO ()
example = putStrLn
        $ unlines
        $ take 7
        $ fmap (blockToStr . getBlock 5)
        $ iterate (extend rule) seed
