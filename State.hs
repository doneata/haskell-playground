-- https://www.reddit.com/r/haskell/comments/7sv28h/im_lost_with_state_monad/dt7pyj6?utm_source=share&utm_medium=web2x&context=3
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE ScopedTypeVariables #-}

module State where

-- What should a "stateful computation" look like?
-- I would expect a function that computes a sum over
-- a list of Ints to look like this.
stateSum :: [Int] -> State Int Int
stateSum = undefined
-- => That is, we have a "sum variable" of type Int, and
--    we "return" a result, also of type Int.

-- In a pure context (outside of a "State" computation),
-- what would you expect a function that
--
-- 1. takes some "State computation",
-- 2. provides you a way to initialize all variables,
-- 3. gives you back all the results of interest (above: both Ints),
--
-- to look like?
--
-- In the case of a
moreGeneral :: State s a
moreGeneral = undefined
--
-- we need to
-- 1. take an initial State s a,
-- 2. initialize s (for instance, we would initialize the
--    sum variable in stateSum to zero),
-- 3. return the final value for s, as well as the result of type a.

runState :: State s a -> s -> (a, s) -- (s, a) would be fine, too.
runState = todo

-- Note that the (s -> (a, s)) part is a State computation in of itself.
-- The important thing to remember here is that for all the common monads,
-- like Writer, Reader, etc., the "run function" is the *inverse* of the
-- value constructor.
-- That is, we have
newtype State  s a = State  { todo      :: s -> (a, s) }
newtype Writer w a = Writer { runWriter ::      (a, w) } -- Writer-only state needs not be initialized.
newtype Reader r a = Reader { runReader :: r -> a      }

-- Remember that these "getter" functions are actually of type
actualType :: Writer w a -> (a, w)
actualType = runWriter

-- Now to implement Functor, Applicative, Monad.
-- It's helpful to make compiler errors more concrete, by giving
-- it some additional information on how we would like things to be named.
-- That is, using the above language extensions:
instance Functor (State s) where -- class Functor (f :: * -> *) => the 'a' is free.
  fmap :: forall a b. (a -> b) -> State s a -> State s b
  fmap = undefined

instance Applicative (State s) where
  pure :: forall a. a -> State s a
  pure = undefined
  (<*>) :: forall a b. State s (a -> b) -> State s a -> State s b
  (<*>) = undefined

-- This is the interesting one for now.
-- Here's "how I think" when implementing it step-by-step.
instance Monad (State s) where
  (>>=) :: forall a b. State s a -> (a -> State s b) -> State s b
  -- (>>=) = undefined
  -- { = Inserting arguments }
  -- s >>= k = undefined -- "k" is a commonly chosen name for a "continuation".
  -- { = How do "States come to be"? By applying "State" to an argument. }
  -- (State f) >>= k = undefined
  -- { = This applies to the return value too! }
  -- State f >>= k = State $ undefined
  -- { = We can make use of "type holes" to make GHC tell us
  --     the type of the missing value. }
  -- State f >>= k = State $ _ -- <- We need a (s -> (b, s)) here.
  -- { = How do "functions come to be"? By introducing a lambda (for instance). }
  -- State f >>= k = State $ \s -> _ -- <- We need a (b, s) here.
  -- { = Now we have stated all that we know so far.
  --     Let's add some type signatures to make things explicit,
  --     and try to plug things together.
  --     Remember that the "_" can be plugged not only with a value,
  --     but also an *expression* of the same type.
  --     A "let ... in ..." is one such expression.}
  State (f :: s -> (a, s)) >>= (k :: a -> State s b) = State $ \s ->
    -- We have an f, but also an s. If it fits, I applies.
    -- let y = f s
    -- { = We know we have a pair, so we can pattern match on it. }
    let (a, s') = f s
    -- => Now we have an 'a' we can shove down the 'k'.
    --     ssb = k a
    -- { = And again, we can pattern match. }
        State (g :: _) = k a
    -- => Can you see that you are almost done now?
    --    What is the type of this 'g' we just got?
    in undefined

-- If you want to implement Functor and Applicative, you can work in a similar manner.
-- Actually, you can *make use of your knowledge* that you have a Monad.
-- That is, you can use do-notation. If you do, you will basically re-implement

-- => You can define fmap = liftM above.
liftM :: Monad m => (a -> b) -> m a -> m b
liftM f ma = undefined

-- => Likewise, (<*>) = ap.
ap :: Monad m => m (a -> b) -> m a -> m b
ap mf ma = undefined


