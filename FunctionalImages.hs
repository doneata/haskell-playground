-- Functional images
-- conal.net/papers/functional-images/fop-conal.pdf

-- Further ideas:
-- - [ ] Render animation as GIF
-- - [ ] Use dependent types to constrain Frac to be in [0, 1].
-- - [ ] Define `lift1` in terms of `fmap` and `lift2` and `lift3` in terms of
--       applicative composition.

{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

import Codec.Picture hiding (Image)

-- § 7.2 What is an image?

type Point      = (Float, Float)
type PolarPoint = (Float, Float)

type Image a = Point -> a
type Region  = Image Bool

type Frac = Float

fromPolar :: PolarPoint -> Point
fromPolar (ro, theta) = (ro * cos theta, ro * sin theta)

toPolar :: Point -> PolarPoint
toPolar p@(x, y) = (distO p, atan2 y x)

vstrip :: Region
vstrip (x, y) = abs(x) <= 1/2

checker :: Region
checker (x, y) = even $ floor x + floor y

rings :: Region
rings = even . floor . fst . toPolar

altRings :: Region
altRings = even . floor . distO

distO :: Point -> Float
distO (x, y) = sqrt $ x ** 2 + y ** 2

polarChecker :: Int -> Region
polarChecker n = checker . sc . toPolar
    where sc (ro, theta) = (ro, theta * fromIntegral n / pi)

wavDist :: Image Frac
wavDist p = (1 + cos (pi * distO p)) / 2

-- § 7.3 Colours

type Colour = (Frac, Frac, Frac, Frac)
type ImageC = Image Colour

invisble = (0.0, 0.0, 0.0, 0.0) :: Colour
black    = (0.0, 0.0, 0.0, 1.0) :: Colour
blue     = (1.0, 0.0, 0.0, 1.0) :: Colour
red      = (0.0, 0.0, 1.0, 1.0) :: Colour
yellow   = (0.0, 1.0, 1.0, 1.0) :: Colour
white    = (1.0, 1.0, 1.0, 1.0) :: Colour

lerpC :: Frac -> Colour -> Colour -> Colour
lerpC w (b1, g1, r1, a1) (b2, g2, r2, a2) = (h b1 b2, h g1 g2, h r1 r2, h a1 a2)
    where h x1 x2 = w * x1 + (1 - w) * x2

-- Ex. 7.1
lighten :: Frac -> Colour -> Colour
lighten w c = lerpC w c white

darkern :: Frac -> Colour -> Colour
darkern w c = lerpC w c black

fade :: Frac -> Colour -> Colour
fade w c@(b, g, r, a) = lerpC w c (b, g, r, 0)

-- Ex. 7.2
-- Example usage:
-- > translate (-0.5, -0.5) (bilerpC black red blue white)
bilerpC :: Colour -> Colour -> Colour -> Colour -> ImageC
bilerpC c1 c2 c3 c4 = \(w1, w2) -> lerpC w2 (lerpC w1 c1 c2) (lerpC w1 c3 c4)

overC :: Colour -> Colour -> Colour
(b1, g1, r1, a1) `overC` (b2, g2, r2, a2) = (h b1 b2, h g1 g2, h r1 r2, h a1 a2)
    where h x1 x2 = x1 + (1 - a1) * x2

-- § 7.4 Point-wise lifting

lift1 :: (a -> b) -> (p -> a) -> (p -> b)
lift1 f im = \p -> f (im p)

lift2 :: (a -> b -> c) -> (p -> a) -> (p -> b) -> (p -> c)
lift2 f im1 im2 = \p -> f (im1 p) (im2 p)

lift3 :: (a -> b -> c -> d) -> (p -> a) -> (p -> b) -> (p -> c) -> (p -> d)
lift3 f im1 im2 im3 = \p -> f (im1 p) (im2 p) (im3 p)

over :: ImageC -> ImageC -> ImageC
over  = lift2 overC

cond :: Image Bool -> Image a -> Image a -> Image a
cond = lift3 (\c x y -> if c then x else y)

lerpI :: Image Frac -> ImageC -> ImageC -> ImageC
lerpI = lift3 lerpC

-- Already provided by Prelude
-- const :: a -> (p -> a)
-- const a = \_ -> a

emptyI  = const invisble
blackI  = const black
blueI   = const blue
redI    = const red
yellowI = const yellow
whiteI  = const white

ybRings = lerpI wavDist blueI yellowI

-- Ex. 7.3
bwIm :: Region -> ImageC
bwIm = lift1 (\b -> if b then white else black)

byIm :: Region -> ImageC
byIm = lift1 (\b -> if b then yellow else blue)

-- Ex. 7.4
ex74 :: ImageC
ex74 = lerpI wavDist (byIm checker) (bwIm $ polarChecker 10)

-- Ex. 7.5
clift1 :: (Frac -> Frac) -> Colour -> Colour
clift1 f (b, g, r, a) = (f b, f g, f r, f a)

clift2 :: (Frac -> Frac -> Frac) -> Colour -> Colour -> Colour
clift2 f (b1, g1, r1, a1) (b2, g2, r2, a2) = (f b1 b2, f g1 g2, f r1 r2, f a1 a2)

lerpC' :: Frac -> Colour -> Colour -> Colour
lerpC' w = clift2 h where h x1 x2 = w * x1 + (1 - w) * x2

overC' :: Colour -> Colour -> Colour
overC' c1@(_, _, _, a) c2 = clift2 h c1 c2 where h x1 x2 = x1 + (1 - a) * x2

-- § 7.5 Spatial transforms

type Transform = Point -> Point
type Vector    = (Float, Float)

translateP :: Vector -> Transform
translateP (dx, dy) (x, y) = (x + dx, y + dy)

scaleP :: Vector -> Transform
scaleP (sx, sy) (x, y) = (sx * x, sy * y)

uscaleP :: Float -> Transform
uscaleP s = scaleP (s, s)

rotateP :: Float -> Transform
rotateP theta (x, y) = (x * cos theta - y * sin theta, y * cos theta + x * sin theta)

swirlP :: Float -> Transform
swirlP r p = rotateP (distO p * 2 * pi / r) p

-- Wrong implementation
-- applyTrans :: Transform -> Image a -> Image a
-- applyTrans xf im = im . xf

udisk :: Region
udisk p = (distO p < 1)

type Filter a = Image a -> Image a
type FilterC  = Filter Colour

translate :: Vector -> Filter a
translate (sx, sy) im = im . translateP (-sx, -sy)

scale :: Vector -> Filter a
scale (sx, sy) im = im . scaleP (1 / sx, 1 / sy)

uscale :: Float -> Filter a
uscale s = scale (s, s)

rotate :: Float -> Filter a
rotate theta im = im . rotateP (-theta)

swirl :: Float -> Filter a
swirl r im = im . swirlP (-r)

-- § 7.6 Animation

type Time   = Float
type Anim a = Time -> Image a

xPos :: Region
xPos (x, y) = x > 0

animSwirl :: Anim Bool
animSwirl t = swirl (t ** 2) xPos

-- § 7.7 Region algebra

universeR :: Region
universeR = const True

emptyR :: Region
emptyR = const False

compR :: Region -> Region
compR = lift1 not

andR :: Region -> Region -> Region
andR = lift2 (&&)

orR :: Region -> Region -> Region
orR = lift2 (||)

xorR :: Region -> Region -> Region
xorR = lift2 (/=)

subR :: Region -> Region -> Region
subR r r' = r `andR` compR r'

annulus :: Frac -> Region
annulus inner = udisk `subR` uscale inner udisk

radReg :: Int -> Region
radReg n = test . toPolar
    where test (_, theta) = even $ floor $ theta * fromIntegral n / pi

wedgeAnnulus :: Frac -> Int -> Region
wedgeAnnulus inner n = annulus inner `andR` radReg n

shiftXor :: Float -> Filter Bool
shiftXor r reg = reg' r `xorR` reg' (-r)
    where reg' t = translate (t, 0) reg

-- Ex. 7.6
xorgon :: Int -> Float -> Filter Bool
xorgon n r reg = foldl1 xorR $ map (\i -> f i reg) [0..n-1]
    where
        theta i = 2.0 * pi * (fromIntegral i) / (fromIntegral n)
        f i = rotate (theta i) . translate (r, 0)

-- Ex. 7.7
polarChecker' :: Int -> Region
polarChecker' n = altRings `xorR` radReg n

-- Ex. 7.8
checker' :: Region
checker' = vstrips `xorR` swapCoord vstrips
    where vstrips (x, _) = even $ floor x

swapCoord :: Filter a
swapCoord im = im . rotateP (pi / 2)

-- Ex. 7.9
crop :: Region -> FilterC
crop reg im = cond reg im emptyI

ex79a :: ImageC
ex79a = crop (wedgeAnnulus 0.25 10) ybRings

ex79b :: ImageC
ex79b = swirl 2 $ crop (wedgeAnnulus 0.25 10) ybRings

-- § 7.8 Some polar transforms
swirlP' :: Float -> Transform
swirlP' r = polarXf $ \(rho, theta) -> (rho, theta + rho * 2 * pi / r)

polarXf :: Transform -> Transform
polarXf xf = fromPolar . xf . toPolar

radInvertP :: Transform
radInvertP = polarXf $ \(rho, theta) -> (1 / rho, theta)

radInvert :: Image a -> Image a
radInvert im = im . radInvertP

rippleRadP :: Int -> Float -> Transform
rippleRadP n s = polarXf $ \(rho, theta) ->
    let alpha = fromIntegral n * theta in
    (rho * (1 + s * sin alpha), theta)

rippleRad :: Int -> Float -> Image a -> Image a
rippleRad n s im = im . rippleRadP n (-s)

cropRad :: Float -> FilterC
cropRad r = crop (uscale r udisk)

-- Ex. 7.10
ex710a :: ImageC
ex710a = cropRad 5 $ rippleRad 8 0.3 ybRings

ex710b :: ImageC
ex710b = cropRad 5 $ swirl 10 $ rippleRad 5 0.3 ybRings

ex710c :: ImageC
ex710c = swirl 10 $ rippleRad 5 0.3 $ cropRad 5 ybRings

circleLimit :: Float -> FilterC
circleLimit radius im = cropRad radius $ im . polarXf xf
    where xf (rho, theta) = (radius * rho / (radius - rho), theta)

-- § Image rendering

fracToPixel8 :: Frac -> Pixel8
fracToPixel8 = floor . (255 *)

class Pixelize a where
    toPixel :: a -> PixelRGBA8

instance Pixelize Bool where
    toPixel True  = PixelRGBA8 0 0 0 255
    toPixel False = PixelRGBA8 255 255 255 255

instance Pixelize Frac where
    toPixel v = let f = fracToPixel8
                in PixelRGBA8 (f v) (f v) (f v) 255

instance Pixelize Colour where
    toPixel (b, g, r, a) = let f = fracToPixel8
                           in PixelRGBA8 (f r) (f g) (f b) (f a)

imageSize = 256

-- Example usage:
-- > render "quilt.png" image 1
render :: Pixelize a => FilePath -> Image a -> Int -> IO()
render path image width = do
    let convertDist d = 
            let w = fromIntegral width
                t = fromIntegral d
                i = fromIntegral imageSize
            in w * ((t + 1.0) / i - 0.5)
        toPixel' x  y = toPixel $ image (convertDist x, convertDist y)
        dynamicImage  = ImageRGBA8 $ generateImage toPixel' imageSize imageSize
    savePngImage path dynamicImage
